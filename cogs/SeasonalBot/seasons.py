import os.path
import asyncio
import contextlib
import datetime
import json

import async_timeout
from discord.ext import commands
from typing import List, Optional, Type, Union

ICON_BASE_URL = "./cogs/SeasonalBot/"


def get_seasons() -> List[str]:
    """Returns all the Season objects located in /bot/seasons/."""

    seasons = []

    with open('./data/seasons.json', 'r') as file:
        seasons = json.load(file)

    return seasons


def get_season(bot, season_name: str = None, date: datetime.datetime = None) -> "SeasonBase":
    """Returns a Season object based on either a string or a date."""

    # If either both or neither are set, raise an error.
    if not bool(season_name) ^ bool(date):
        raise UserWarning("This function requires either a season or a date in order to run.")

    seasons = get_seasons()
    season_classes = []

    # If name provided grab the specified class or fallback to evergreen.
    if season_name:
        season_name = season_name.lower()
        for season in seasons:
            if season["name"] == season_name:
                season_class = SeasonBase(bot, **season)
                season_classes.append(season_class)
        else:
            if not season_classes:
                evergreen_season = next(season for season in seasons if season['name'] == 'evergreen')
                evergreen_class = SeasonBase(bot, **evergreen_season)
                return [evergreen_class]

    # Else match by dates
    else:
        for season in seasons:
            if 'evergreen' not in season["name"]:
                season_class = SeasonBase(bot, **season)
                if season_class.is_between_dates(date):
                    season_classes.append(season_class)
        else:
            if not season_classes:
                print("load evergreen")
                evergreen_classes = [SeasonBase(bot, **season) for season in seasons if 'evergreen' in season['name']]
                return evergreen_classes

    return season_classes


class SeasonalBot(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.season = get_season(bot, date=datetime.datetime.utcnow())
        self.season_task = bot.loop.create_task(self.load_seasons())

        # Figure out number of seconds until a minute past midnight
        tomorrow = datetime.datetime.now() + datetime.timedelta(1)
        midnight = datetime.datetime(
            year=tomorrow.year,
            month=tomorrow.month,
            day=tomorrow.day,
            hour=0,
            minute=0,
            second=0
        )
        self.sleep_time = (midnight - datetime.datetime.now()).seconds + 60

    async def load_seasons(self):
        """Asynchronous timer loop to check for a new season every midnight."""

        await self.bot.wait_until_ready()
        for guild_season in self.season:
            await guild_season.load()

        while True:
            await asyncio.sleep(self.sleep_time)  # sleep until midnight
            self.sleep_time = 86400  # next time, sleep for 24 hours.

            # If the season has changed, load it.
            new_season = get_season(self.bot, date=datetime.datetime.utcnow())
            for guild_season in self.season:
                await guild_season.load()


class SeasonBase:
    """Base class for Seasonal classes."""

    def __init__(self, bot,
                 guild_id,
                 name, bot_name,
                 start_date, end_date,
                 colour, icon, bot_icon,
                 date_format):

        self.bot = bot
        self.guild_id = guild_id

        self.name = name
        self.bot_name = bot_name

        self.start_date = start_date
        self.end_date = end_date

        self.colour = colour
        self.icon = icon
        self.bot_icon = bot_icon

        self.date_format = date_format

    @staticmethod
    def current_year() -> int:
        """Returns the current year."""

        return datetime.date.today().year

    def start(self) -> datetime.datetime:
        """
        Returns the start date using current year and start_date attribute.
        If no start_date was defined, returns the minimum datetime to ensure it's always below checked dates.
        """

        if not self.start_date:
            return datetime.datetime.min
        return datetime.datetime.strptime(f"{self.start_date}/{self.current_year()}", self.date_format)

    def end(self) -> datetime.datetime:
        """
        Returns the start date using current year and end_date attribute.
        If no end_date was defined, returns the minimum datetime to ensure it's always above checked dates.
        """

        if not self.end_date:
            return datetime.datetime.max
        return datetime.datetime.strptime(f"{self.end_date}/{self.current_year()}", self.date_format)

    def is_between_dates(self, date: datetime.datetime) -> bool:
        """Determines if the given date falls between the season's date range."""

        return self.start() <= date <= self.end()

    @property
    def name_clean(self) -> str:
        """Return the Season's name with underscores replaced by whitespace."""

        return self.name.replace("_", " ").title()

    async def get_icon(self) -> bytes:
        """
        Retrieve the season's icon from the branding repository using the Season's icon attribute.
        If `avatar` is True, uses optional bot-only avatar icon if present.
        The icon attribute must provide the url path, starting from the master branch base url,
        including the starting slash.
        e.g. `/logos/logo_seasonal/valentines/loved_up.png`
        """

        full_url = ICON_BASE_URL + self.icon
        print(f"Getting icon from: {full_url}")
        print("File exists?", os.path.isfile(full_url))
        with open(full_url, 'rb') as resp:
            return resp.read()

    async def apply_username(self):
        """
        Applies the username for the current season.
        """

        guild = self.bot.get_guild(self.guild_id)
        changed = False

        if guild.me.name != self.bot_name and guild.me.nick != self.bot_name:
            # attempt to change user details
            print(f"Changing nick to {self.bot_name}")
            await guild.me.edit(nick=self.bot_name)
            changed = True

        # remove nickname if an old one exists
        if guild.me.nick and guild.me.nick != self.bot_name:
            print(f"Clearing old nickname of {guild.me.nick}")
            await guild.me.edit(nick=None)

        return changed

    async def apply_server_icon(self) -> bool:
        """
        Applies the server icon for the current season.
        Returns True if was successful.
        """

        guild = self.bot.get_guild(self.guild_id)

        # track old icon hash for later comparison
        old_icon = guild.icon

        # attempt the change
        print(f"Changing server icon to {self.icon} in guild {guild}")

        if self.icon is not None:
            icon = await self.get_icon()
        else:
            icon = None

        print("Attempt to edit the server icon")
        try:
            await guild.edit(icon=icon, reason=f"Seasonbot Season Change: {self.name}")
        except asyncio.TimeoutError:
            print("changing guild icon timed out")

        new_icon = self.bot.get_guild(self.guild_id).icon
        if new_icon != old_icon:
            print(f"Server icon changed to {self.icon} in guild {guild}")
            return True

        print(f"Changing server icon failed: {self.icon} in guild {guild}")
        return False

    async def load(self):
        """
        Loads extensions, bot name and avatar, server icon and announces new season.
        If in debug mode, the avatar, server icon, and announcement will be skipped.
        """

        guild = self.bot.get_guild(self.guild_id)

        print()
        print("------[ SeasonalBot ]------")
        print(f"start loading season {self.name} for {guild}")
        # Apply seasonal elements after extensions successfully load
        username_changed = await self.apply_username()

        """
        if username_changed:
            print(f"Applying server icon to guild {guild}.")
            await self.apply_server_icon()
        else:
            print(f"Skipping server icon change in guild {guild} due to username not being changed.")
        """
        print(f"Applying server icon to guild {guild}.")
        await self.apply_server_icon()

        print(f"season {self.name} loaded successfully in guild {guild}")
        print("------[ SeasonalBot ]------")
        print()


def setup(bot):
    bot.add_cog(SeasonalBot(bot))

import discord
from discord.ext import commands
from utils import dict_factory, isOwner


class Color(commands.Cog):

    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    @isOwner()
    def setup_colors(self, ctx):
        print(self.bot.emojis)


def setup(bot):
    bot.add_cog(Color(bot))

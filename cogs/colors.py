import discord
from discord.ext import commands
from utils import isOwner
import requests
import json
import io
from PIL import Image


class Color(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

        with open("./data/color_manager.json", 'r') as fileR:
            self.color_manager = json.load(fileR)

    @isOwner()
    @commands.command()
    async def setupColors(self, ctx):
        await ctx.message.delete()

        emojis = list(filter(lambda emoji: emoji.name.startswith("color_"), self.bot.emojis))
        file = discord.File("./data/divider.gif", filename="divider.gif")

        await ctx.send("**Pick your color**")
        await ctx.send(file=file)
        for emoji in emojis:
            await ctx.send("`{0.name:<50}:` {0}".format(emoji))
        last_message = await ctx.send(file=file)

        for emoji in emojis:
            await last_message.add_reaction(emoji)

        self.color_manager[ctx.guild.id] = {"channel_id": ctx.channel.id, "message_id": last_message.id}
        with open("./data/color_manager.json", 'w') as fileW:
            fileW.write(json.dumps(self.color_manager))

    @isOwner()
    @commands.command()
    @commands.has_permissions(manage_emojis=True, manage_roles=True)
    async def addColor(self, ctx, hex_color, addEmoji=False, addRole=False):
        await ctx.message.delete()

        hex_color = hex_color.strip("#")

        try:
            color_response = requests.get("http://www.thecolorapi.com/id?hex={}".format(hex_color))
            color = json.loads(color_response.content)
            name = "color_" + color["name"]["value"].replace(" ", "_")

            R, G, B = tuple(int(hex_color[i:i + 2], 16) for i in (0, 2, 4))
            img = Image.new("RGB", (128, 128), (R, G, B))

            imgByteArr = io.BytesIO()
            img.save(imgByteArr, format='PNG')
            imgByteArr = imgByteArr.getvalue()

        except (requests.exceptions.MissingSchema, requests.exceptions.InvalidURL, requests.exceptions.InvalidSchema, requests.exceptions.ConnectionError):
            return await ctx.send("The URL you have provided is invalid.")

        if color_response.status_code == 404:
            return await ctx.send("The URL you have provided leads to a 404.")

        try:
            if addEmoji:
                emoji = await ctx.guild.create_custom_emoji(name=name, image=imgByteArr)

            if addRole:
                role = await ctx.guild.create_role(name=name, colour=discord.Colour.from_rgb(R, G, B))

        except discord.InvalidArgument:
            return await ctx.send("Invalid image type. Only PNG, JPEG and GIF are supported.")

        if addEmoji:
            await ctx.send("Successfully added the emoji {0.name} <{1}:{0.name}:{0.id}>!".format(emoji, "a" if emoji.animated else ""))

        if addRole:
            await ctx.send("Successfully added role {0.name}".format(role))

        data = self.color_manager[str(ctx.guild.id)]
        channel = discord.utils.get(ctx.guild.channels, id=data["channel_id"])
        react_message = await channel.get_message(data["message_id"])

        if not addEmoji:
            emojis = [x for x in self.bot.emojis if x.name.lower() == name]
            if len(emojis) > 0:
                await react_message.add_reaction(emojis[0])

    @isOwner()
    @commands.command()
    @commands.has_permissions(manage_emojis=True, manage_roles=True)
    async def removeColor(self, ctx, name, removeEmoji=False, removeRole=False):
        await ctx.message.delete()

        name = name.strip(":")
        if not name.startswith("color_"):
            name = "color_" + name

        emotes = [x for x in ctx.guild.emojis if x.name.lower() == name.lower()]
        if removeEmoji:
            if not emotes:
                return await ctx.send("No emotes with that name could be found on this server.")
            for emote in emotes:
                await emote.delete()
            if len(emotes) == 1:
                await ctx.send("Successfully removed the {} emoji!".format(name))
            else:
                await ctx.send("Successfully removed {} emoji with the name {}.".format(len(emotes), name))

        roles = [x for x in ctx.guild.roles if x.name.lower() == name.lower()]
        if removeRole:
            if not roles:
                return await ctx.send("No roles with that name could be found on this server.")
            for role in roles:
                await role.delete()
            if len(roles) == 1:
                await ctx.send("Successfully removed the {} role!".format(name))
            else:
                await ctx.send("Successfully removed {} role with the name {}.".format(len(roles), name))

        data = self.color_manager[str(ctx.guild.id)]
        channel = discord.utils.get(ctx.guild.channels, id=data["channel_id"])
        react_message = await channel.get_message(data["message_id"])

        emotes = list(filter(lambda react: react.emoji.name.lower() == name, react_message.reactions))
        for emote in emotes:
            async for user in emote.users():
                await react_message.remove_reaction(emote, user)

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload):
        guild = discord.utils.get(self.bot.guilds, id=payload.guild_id)
        channel = discord.utils.get(guild.text_channels, id=payload.channel_id)
        message = await channel.fetch_message(payload.message_id)
        user = discord.utils.get(guild.members, id=payload.user_id)
        emoji = payload.emoji

        if (str(payload.guild_id) in self.color_manager and
                payload.message_id == self.color_manager[str(payload.guild_id)]["message_id"]):

            print(user, "added reaction", emoji.name, "to message")
            name = emoji.name
            roles = [x for x in message.guild.roles if x.name.lower() == name.lower()]

            if len(roles) > 0:
                await user.add_roles(roles[0])

    @commands.Cog.listener()
    async def on_raw_reaction_remove(self, payload):
        guild = discord.utils.get(self.bot.guilds, id=payload.guild_id)
        channel = discord.utils.get(guild.text_channels, id=payload.channel_id)
        message = await channel.fetch_message(payload.message_id)
        user = discord.utils.get(guild.members, id=payload.user_id)
        emoji = payload.emoji

        if (str(payload.guild_id) in self.color_manager and
                payload.message_id == self.color_manager[str(payload.guild_id)]["message_id"]):

            print(user, "removed reaction", emoji.name, "from message")
            name = emoji.name
            roles = [x for x in message.guild.roles if x.name.lower() == name.lower()]

            if len(roles) > 0:
                await user.remove_roles(roles[0])


def setup(bot):
    bot.add_cog(Color(bot))

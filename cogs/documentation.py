import discord
from discord.ext import commands

from email.parser import HeaderParser
from io import StringIO
import aiohttp
import autopep8
import asyncio


class Documentation(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

        self.base_pep_url = "http://www.python.org/dev/peps/pep-"
        self.base_github_pep_url = "https://raw.githubusercontent.com/python/peps/master/pep-"

    @commands.command(name='pep', aliases=('get_pep', 'p'))
    async def pep_command(self, ctx, pep_number: str):
        """
        Fetches information about a PEP and sends it to the channel.
        """

        if pep_number.isdigit():
            pep_number = int(pep_number)
        else:
            return await ctx.invoke(self.bot.get_command("help"), "pep")

        # Newer PEPs are written in RST instead of txt
        if pep_number > 542:
            pep_url = f"{self.base_github_pep_url}{pep_number:04}.rst"
        else:
            pep_url = f"{self.base_github_pep_url}{pep_number:04}.txt"

        # Attempt to fetch the PEP
        """
        response = await self.bot.http_session.get(pep_url)
        """

        async with aiohttp.ClientSession() as sess:
            async with sess.get(pep_url) as response:

                if response.status == 200:
                    pep_content = await response.text()

                    # Taken from https://github.com/python/peps/blob/master/pep0/pep.py#L179
                    pep_header = HeaderParser().parse(StringIO(pep_content))

                    # Assemble the embed
                    pep_embed = discord.Embed(
                        title=f"**PEP {pep_number} - {pep_header['Title']}**",
                        description=f"[Link]({self.base_pep_url}{pep_number:04})",
                    )

                    pep_embed.set_thumbnail(url="https://www.python.org/static/opengraph-icon-200x200.png")

                    # Add the interesting information
                    if "Status" in pep_header:
                        pep_embed.add_field(name="Status", value=pep_header["Status"])
                    if "Python-Version" in pep_header:
                        pep_embed.add_field(name="Python-Version", value=pep_header["Python-Version"])
                    if "Created" in pep_header:
                        pep_embed.add_field(name="Created", value=pep_header["Created"])
                    if "Type" in pep_header:
                        pep_embed.add_field(name="Type", value=pep_header["Type"])

                elif response.status == 404:
                    not_found = f"PEP {pep_number} does not exist."
                    pep_embed = discord.Embed(title="PEP not found", description=not_found)
                    pep_embed.colour = discord.Colour.red()

                else:
                    error_message = "Unexpected HTTP error during PEP search. Please let us know."
                    pep_embed = discord.Embed(title="Unexpected error", description=error_message)
                    pep_embed.colour = discord.Colour.red()

                react_message = await ctx.message.channel.send(embed=pep_embed)

                for emoji in ('❌'):
                    await react_message.add_reaction(emoji)

    @staticmethod
    def get_lang(code_block: str) -> str:
        """Returns the language specified for the markup of a codeblock."""

        to_newline = code_block[3:code_block.find('\n')]
        return to_newline.replace(' ', '').replace('\n', '')

    @commands.command(aliases=["pepfix", "lint", "pylint"])
    async def fixpep(self, ctx, *, code_block):
        lang = self.get_lang(code_block)

        code = code_block.strip('`')
        if code.startswith(lang):
            code = code[len(lang):]
        code = code.strip()

        fixed_code = autopep8.fix_code(code, options={'aggressive': 1})

        await ctx.send('```python\n' + fixed_code + '```')


def setup(bot):
    bot.add_cog(Documentation(bot))

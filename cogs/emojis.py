import discord
from discord.ext import commands
import requests
import json
import re
import emoji


class Emoji_cog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    async def on_message(self, message):
        if message.guild is None:
            return

        guild_id = message.guild.id
        author_id = message.author.id

        match = re.findall("\:([a-zA-Z0-9_]+)\:", emoji.demojize(message.content))
        if match:
            values = []
            for emoji_char in match:
                values.append((guild_id, author_id, emoji_char))

            requests.get('http://193.87.105.215/webs/discordBot/query_executor.php', params={
                "sql": "INSERT INTO `emojis` VALUES " + ",".join(map(str, values))})

    async def on_reaction_add(self, reaction, user):
        message = reaction.message

        guild_id = message.guild.id
        author_id = message.author.id
        emoji_char = ":{}:".format(reaction.emoji.name) if hasattr(reaction.emoji, 'name') else emoji.demojize(reaction.emoji)

        requests.get('http://193.87.105.215/webs/discordBot/query_executor.php', params={
            "sql": "INSERT INTO `emojis` VALUES " + f"({guild_id}, {author_id}, {emoji_char})"})

    @commands.command()
    @commands.cooldown(rate=1, per=500, type=commands.BucketType.channel)
    async def topEmojis(self, ctx, topN=20, guild_id=None):
        message = ctx.message
        channel = message.channel
        guild_id = int(guild_id if guild_id else message.guild.id)

        response = requests.get('http://193.87.105.215/webs/discordBot/query_executor.php', params={
            "sql": "SELECT `guild_id`, `emoji`, COUNT(`emoji`) AS `count` FROM `emojis` GROUP BY `emoji` ORDER BY `count` DESC"})

        message_data = json.loads(response.content)

        inguild = list(filter(lambda message: int(message["guild_id"]) == guild_id, message_data))

        out = ""
        for message in inguild:
            if int(message['count']) >= 8:
                out += "{emoji_char}\t{emoji_count}\n".format(
                    emoji_char=emoji.emojize(":" + message['emoji'] + ":"),
                    emoji_count=message['count']
                )

        print("creating topEmojis pages")
        pager = commands.Paginator(prefix='', suffix='', max_size=1595)
        for line in out.split("\n")[:topN]:
            if len(line) > 1595:
                continue
            pager.add_line(line)

        print("topEmojis, sending...")
        for page in pager.pages:
            await channel.send(page)

        """
        message = ctx.message
        channel = message.channel
        guild_id = int(guild_id if guild_id else message.guild.id)

        patience = await channel.send("It will take a while... please be patient")

        with open("data/leaderboard_all.json") as file:
            messages = json.load(file)

            emoji_pattern = "\:[^\s\:]+\:"

            emoji_counter = {}
            inguild = list(filter(lambda message: message["guild_id"] == guild_id, messages))

            for msg in inguild:
                author = msg["author"]

                matched_emojis = re.findall(emoji_pattern, msg["content"], flags=re.IGNORECASE)
                emojis = list(zip(matched_emojis, [author] * len(matched_emojis)))

                for react in msg["reactions"]:
                    matched_emoji = re.findall(emoji_pattern, react["name"])
                    for i in range(react["count"]):
                        emojis += list(zip(matched_emoji, [react["users"][i]]))

                for emoji, user in emojis:
                    emoji_counter.setdefault(emoji, {})
                    emoji_counter[emoji].setdefault("count", 0)
                    emoji_counter[emoji].setdefault("users", {})
                    emoji_counter[emoji]["users"].setdefault(user, 0)

                    emoji_counter[emoji]["count"] += 1
                    emoji_counter[emoji]["users"][user] += 1

        out = ""
        for emoji in sorted(emoji_counter,
                            key=lambda emoji: emoji_counter[emoji]["count"],
                            reverse=True)[:topN]:

            found_emoji = list(filter(lambda em: em.name.lower().strip(":") == emoji.lower().strip(":"), self.bot.emojis))
            emoji_char = (
                found_emoji[0]
                if len(found_emoji) >= 1 else
                emoji
            )
            emoji_count = emoji_counter[emoji]["count"]
            sorted_spammers = sorted(emoji_counter[emoji]["users"],
                                     key=lambda user: emoji_counter[emoji]["users"][user],
                                     reverse=True)

            spammer_names = sorted_spammers[0]
            index = 1
            while index < len(sorted_spammers) and emoji_counter[emoji]["users"][sorted_spammers[0]] == emoji_counter[emoji]["users"][sorted_spammers[index]]:
                spammer_names += " + " + sorted_spammers[index]
                index += 1

            if emoji_count >= 8:
                out += f"{emoji_char}\t{emoji_count}\t{spammer_names}\n"

        await patience.delete()
        if len(out) == 0:
            await channel.send("no emojis found in this guild")
            return

        pager = commands.Paginator(prefix='', suffix='', max_size=1595)
        for line in out.split("\n"):
            if len(line) > 1595:
                continue
            pager.add_line(line)

        for page in pager.pages:
            await channel.send(page)
        """

        # for message in messages:
        #    await channel.send(message)


def setup(bot):
    bot.add_cog(Emoji_cog(bot))

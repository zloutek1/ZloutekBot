import discord
from discord.ext import commands
from typing import Optional
from random import choice, shuffle, random
import requests
import json

import PIL
import PIL.Image
import PIL.ImageFont
import PIL.ImageOps
import PIL.ImageDraw

from pyfiglet import figlet_format
from io import BytesIO
from glob import glob


class Fun(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

        self.suicideMessages = self._loadLinestAsList("data/suicide.txt")
        self.ASCII_FONT = [
            [0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00],
            [0x00, 0x3E, 0x41, 0x55, 0x41, 0x55, 0x49, 0x3E],
            [0x00, 0x3E, 0x7F, 0x6B, 0x7F, 0x6B, 0x77, 0x3E],
            [0x00, 0x22, 0x77, 0x7F, 0x7F, 0x3E, 0x1C, 0x08],
            [0x00, 0x08, 0x1C, 0x3E, 0x7F, 0x3E, 0x1C, 0x08],
            [0x00, 0x08, 0x1C, 0x2A, 0x7F, 0x2A, 0x08, 0x1C],
            [0x00, 0x08, 0x1C, 0x3E, 0x7F, 0x3E, 0x08, 0x1C],
            [0x00, 0x00, 0x1C, 0x3E, 0x3E, 0x3E, 0x1C, 0x00],
            [0xFF, 0xFF, 0xE3, 0xC1, 0xC1, 0xC1, 0xE3, 0xFF],
            [0x00, 0x00, 0x1C, 0x22, 0x22, 0x22, 0x1C, 0x00],
            [0xFF, 0xFF, 0xE3, 0xDD, 0xDD, 0xDD, 0xE3, 0xFF],
            [0x00, 0x0F, 0x03, 0x05, 0x39, 0x48, 0x48, 0x30],
            [0x00, 0x08, 0x3E, 0x08, 0x1C, 0x22, 0x22, 0x1C],
            [0x00, 0x18, 0x14, 0x10, 0x10, 0x30, 0x70, 0x60],
            [0x00, 0x0F, 0x19, 0x11, 0x13, 0x37, 0x76, 0x60],
            [0x00, 0x08, 0x2A, 0x1C, 0x77, 0x1C, 0x2A, 0x08],
            [0x00, 0x60, 0x78, 0x7E, 0x7F, 0x7E, 0x78, 0x60],
            [0x00, 0x03, 0x0F, 0x3F, 0x7F, 0x3F, 0x0F, 0x03],
            [0x00, 0x08, 0x1C, 0x2A, 0x08, 0x2A, 0x1C, 0x08],
            [0x00, 0x66, 0x66, 0x66, 0x66, 0x00, 0x66, 0x66],
            [0x00, 0x3F, 0x65, 0x65, 0x3D, 0x05, 0x05, 0x05],
            [0x00, 0x0C, 0x32, 0x48, 0x24, 0x12, 0x4C, 0x30],
            [0x00, 0x00, 0x00, 0x00, 0x00, 0x7F, 0x7F, 0x7F],
            [0x00, 0x08, 0x1C, 0x2A, 0x08, 0x2A, 0x1C, 0x3E],
            [0x00, 0x08, 0x1C, 0x3E, 0x7F, 0x1C, 0x1C, 0x1C],
            [0x00, 0x1C, 0x1C, 0x1C, 0x7F, 0x3E, 0x1C, 0x08],
            [0x00, 0x08, 0x0C, 0x7E, 0x7F, 0x7E, 0x0C, 0x08],
            [0x00, 0x08, 0x18, 0x3F, 0x7F, 0x3F, 0x18, 0x08],
            [0x00, 0x00, 0x00, 0x70, 0x70, 0x70, 0x7F, 0x7F],
            [0x00, 0x00, 0x14, 0x22, 0x7F, 0x22, 0x14, 0x00],
            [0x00, 0x08, 0x1C, 0x1C, 0x3E, 0x3E, 0x7F, 0x7F],
            [0x00, 0x7F, 0x7F, 0x3E, 0x3E, 0x1C, 0x1C, 0x08],
            [0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00],
            [0x00, 0x18, 0x3C, 0x3C, 0x18, 0x18, 0x00, 0x18],
            [0x00, 0x36, 0x36, 0x14, 0x00, 0x00, 0x00, 0x00],
            [0x00, 0x36, 0x36, 0x7F, 0x36, 0x7F, 0x36, 0x36],
            [0x00, 0x08, 0x1E, 0x20, 0x1C, 0x02, 0x3C, 0x08],
            [0x00, 0x60, 0x66, 0x0C, 0x18, 0x30, 0x66, 0x06],
            [0x00, 0x3C, 0x66, 0x3C, 0x28, 0x65, 0x66, 0x3F],
            [0x00, 0x18, 0x18, 0x18, 0x30, 0x00, 0x00, 0x00],
            [0x00, 0x06, 0x0C, 0x18, 0x18, 0x18, 0x0C, 0x06],
            [0x00, 0x60, 0x30, 0x18, 0x18, 0x18, 0x30, 0x60],
            [0x00, 0x00, 0x36, 0x1C, 0x7F, 0x1C, 0x36, 0x00],
            [0x00, 0x00, 0x08, 0x08, 0x3E, 0x08, 0x08, 0x00],
            [0x00, 0x00, 0x00, 0x00, 0x30, 0x30, 0x30, 0x60],
            [0x00, 0x00, 0x00, 0x00, 0x3C, 0x00, 0x00, 0x00],
            [0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60, 0x60],
            [0x00, 0x00, 0x06, 0x0C, 0x18, 0x30, 0x60, 0x00],
            [0x00, 0x3C, 0x66, 0x6E, 0x76, 0x66, 0x66, 0x3C],
            [0x00, 0x18, 0x18, 0x38, 0x18, 0x18, 0x18, 0x7E],
            [0x00, 0x3C, 0x66, 0x06, 0x0C, 0x30, 0x60, 0x7E],
            [0x00, 0x3C, 0x66, 0x06, 0x1C, 0x06, 0x66, 0x3C],
            [0x00, 0x0C, 0x1C, 0x2C, 0x4C, 0x7E, 0x0C, 0x0C],
            [0x00, 0x7E, 0x60, 0x7C, 0x06, 0x06, 0x66, 0x3C],
            [0x00, 0x3C, 0x66, 0x60, 0x7C, 0x66, 0x66, 0x3C],
            [0x00, 0x7E, 0x66, 0x0C, 0x0C, 0x18, 0x18, 0x18],
            [0x00, 0x3C, 0x66, 0x66, 0x3C, 0x66, 0x66, 0x3C],
            [0x00, 0x3C, 0x66, 0x66, 0x3E, 0x06, 0x66, 0x3C],
            [0x00, 0x00, 0x18, 0x18, 0x00, 0x18, 0x18, 0x00],
            [0x00, 0x00, 0x18, 0x18, 0x00, 0x18, 0x18, 0x30],
            [0x00, 0x06, 0x0C, 0x18, 0x30, 0x18, 0x0C, 0x06],
            [0x00, 0x00, 0x00, 0x3C, 0x00, 0x3C, 0x00, 0x00],
            [0x00, 0x60, 0x30, 0x18, 0x0C, 0x18, 0x30, 0x60],
            [0x00, 0x3C, 0x66, 0x06, 0x1C, 0x18, 0x00, 0x18],
            [0x00, 0x38, 0x44, 0x5C, 0x58, 0x42, 0x3C, 0x00],
            [0x00, 0x3C, 0x66, 0x66, 0x7E, 0x66, 0x66, 0x66],
            [0x00, 0x7C, 0x66, 0x66, 0x7C, 0x66, 0x66, 0x7C],
            [0x00, 0x3C, 0x66, 0x60, 0x60, 0x60, 0x66, 0x3C],
            [0x00, 0x7C, 0x66, 0x66, 0x66, 0x66, 0x66, 0x7C],
            [0x00, 0x7E, 0x60, 0x60, 0x7C, 0x60, 0x60, 0x7E],
            [0x00, 0x7E, 0x60, 0x60, 0x7C, 0x60, 0x60, 0x60],
            [0x00, 0x3C, 0x66, 0x60, 0x60, 0x6E, 0x66, 0x3C],
            [0x00, 0x66, 0x66, 0x66, 0x7E, 0x66, 0x66, 0x66],
            [0x00, 0x3C, 0x18, 0x18, 0x18, 0x18, 0x18, 0x3C],
            [0x00, 0x1E, 0x0C, 0x0C, 0x0C, 0x6C, 0x6C, 0x38],
            [0x00, 0x66, 0x6C, 0x78, 0x70, 0x78, 0x6C, 0x66],
            [0x00, 0x60, 0x60, 0x60, 0x60, 0x60, 0x60, 0x7E],
            [0x00, 0x63, 0x77, 0x7F, 0x6B, 0x63, 0x63, 0x63],
            [0x00, 0x63, 0x73, 0x7B, 0x6F, 0x67, 0x63, 0x63],
            [0x00, 0x3C, 0x66, 0x66, 0x66, 0x66, 0x66, 0x3C],
            [0x00, 0x7C, 0x66, 0x66, 0x66, 0x7C, 0x60, 0x60],
            [0x00, 0x3C, 0x66, 0x66, 0x66, 0x6E, 0x3C, 0x06],
            [0x00, 0x7C, 0x66, 0x66, 0x7C, 0x78, 0x6C, 0x66],
            [0x00, 0x3C, 0x66, 0x60, 0x3C, 0x06, 0x66, 0x3C],
            [0x00, 0x7E, 0x5A, 0x18, 0x18, 0x18, 0x18, 0x18],
            [0x00, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x3E],
            [0x00, 0x66, 0x66, 0x66, 0x66, 0x66, 0x3C, 0x18],
            [0x00, 0x63, 0x63, 0x63, 0x6B, 0x7F, 0x77, 0x63],
            [0x00, 0x63, 0x63, 0x36, 0x1C, 0x36, 0x63, 0x63],
            [0x00, 0x66, 0x66, 0x66, 0x3C, 0x18, 0x18, 0x18],
            [0x00, 0x7E, 0x06, 0x0C, 0x18, 0x30, 0x60, 0x7E],
            [0x00, 0x1E, 0x18, 0x18, 0x18, 0x18, 0x18, 0x1E],
            [0x00, 0x00, 0x60, 0x30, 0x18, 0x0C, 0x06, 0x00],
            [0x00, 0x78, 0x18, 0x18, 0x18, 0x18, 0x18, 0x78],
            [0x00, 0x08, 0x14, 0x22, 0x41, 0x00, 0x00, 0x00],
            [0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7F],
            [0x00, 0x0C, 0x0C, 0x06, 0x00, 0x00, 0x00, 0x00],
            [0x00, 0x00, 0x00, 0x3C, 0x06, 0x3E, 0x66, 0x3E],
            [0x00, 0x60, 0x60, 0x60, 0x7C, 0x66, 0x66, 0x7C],
            [0x00, 0x00, 0x00, 0x3C, 0x66, 0x60, 0x66, 0x3C],
            [0x00, 0x06, 0x06, 0x06, 0x3E, 0x66, 0x66, 0x3E],
            [0x00, 0x00, 0x00, 0x3C, 0x66, 0x7E, 0x60, 0x3C],
            [0x00, 0x1C, 0x36, 0x30, 0x30, 0x7C, 0x30, 0x30],
            [0x00, 0x00, 0x3E, 0x66, 0x66, 0x3E, 0x06, 0x3C],
            [0x00, 0x60, 0x60, 0x60, 0x7C, 0x66, 0x66, 0x66],
            [0x00, 0x00, 0x18, 0x00, 0x18, 0x18, 0x18, 0x3C],
            [0x00, 0x0C, 0x00, 0x0C, 0x0C, 0x6C, 0x6C, 0x38],
            [0x00, 0x60, 0x60, 0x66, 0x6C, 0x78, 0x6C, 0x66],
            [0x00, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18],
            [0x00, 0x00, 0x00, 0x63, 0x77, 0x7F, 0x6B, 0x6B],
            [0x00, 0x00, 0x00, 0x7C, 0x7E, 0x66, 0x66, 0x66],
            [0x00, 0x00, 0x00, 0x3C, 0x66, 0x66, 0x66, 0x3C],
            [0x00, 0x00, 0x7C, 0x66, 0x66, 0x7C, 0x60, 0x60],
            [0x00, 0x00, 0x3C, 0x6C, 0x6C, 0x3C, 0x0D, 0x0F],
            [0x00, 0x00, 0x00, 0x7C, 0x66, 0x66, 0x60, 0x60],
            [0x00, 0x00, 0x00, 0x3E, 0x40, 0x3C, 0x02, 0x7C],
            [0x00, 0x00, 0x18, 0x18, 0x7E, 0x18, 0x18, 0x18],
            [0x00, 0x00, 0x00, 0x66, 0x66, 0x66, 0x66, 0x3E],
            [0x00, 0x00, 0x00, 0x00, 0x66, 0x66, 0x3C, 0x18],
            [0x00, 0x00, 0x00, 0x63, 0x6B, 0x6B, 0x6B, 0x3E],
            [0x00, 0x00, 0x00, 0x66, 0x3C, 0x18, 0x3C, 0x66],
            [0x00, 0x00, 0x00, 0x66, 0x66, 0x3E, 0x06, 0x3C],
            [0x00, 0x00, 0x00, 0x3C, 0x0C, 0x18, 0x30, 0x3C],
            [0x00, 0x0E, 0x18, 0x18, 0x30, 0x18, 0x18, 0x0E],
            [0x00, 0x18, 0x18, 0x18, 0x00, 0x18, 0x18, 0x18],
            [0x00, 0x70, 0x18, 0x18, 0x0C, 0x18, 0x18, 0x70],
            [0x00, 0x00, 0x00, 0x3A, 0x6C, 0x00, 0x00, 0x00],
            [0x00, 0x08, 0x1C, 0x36, 0x63, 0x41, 0x41, 0x7F]
        ]

    @commands.command()
    async def reactRulette(self, ctx, lastMessageRange: Optional[int] = 10):
        message = ctx.message
        channel = message.channel

        rulette = []
        async for msg in channel.history(limit=lastMessageRange):
            rulette.append(msg)

        winner = choice(rulette)

        emojis = self.bot.emojis
        shuffle(emojis)

        for emoji in emojis:
            await winner.add_reaction(emoji)

    def _loadLinestAsList(self, path):
        with open(path) as file:
            options = []
            for line in file:
                options.append(line)

        return options

    @commands.command()
    async def suicide(self, ctx):
        await ctx.message.channel.send(choice(self.suicideMessages))

    @commands.command()
    @commands.cooldown(rate=1, per=600, type=commands.BucketType.guild)
    async def nightsky(self, ctx):
        await ctx.send("\n".join(['.\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000 ✦ \u3000\u3000\u3000\u3000\u2002\u2002 \u3000', '\u3000\u3000\u3000˚\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000*\u3000\u3000\u3000\u3000\u3000\u3000\u2008', '\u2008\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000.\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000.', '\u3000\u3000\u2008\u3000\u3000\u3000\u3000\u3000\u3000\u3000 ✦ \u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000 \u3000 \u200d \u200d \u200d \u200d \u3000\u3000\u3000\u3000 \u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000,\u3000\u3000\u2002\u2002\u2002\u3000', '', '.\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000.\u3000\u3000\u3000ﾟ\u3000\u2002\u2002\u3000\u3000\u3000.\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000.', '', '\u3000\u3000\u3000\u3000\u3000\u3000,\u3000\u3000\u3000\u3000\u3000\u3000\u3000.\u3000\u3000\u3000\u3000\u3000\u3000\u2008\u2008\u2008\u2008\u3000\u3000\u3000\u3000:alien: doot doot', '\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u2008\u2008', '\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000:sunny:\u3000\u3000\u2008\u2008\u200a\u200a\u3000\u2008\u2008\u2008\u2008\u2008\u200a\u3000\u3000\u3000\u3000\u3000\u2008\u2008\u200a\u200a\u2008\u2008\u200a\u200a\u3000\u3000\u3000\u3000*\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000.', '\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000.\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000.', '\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u200a\u200a\u200a\u2008\u2008\u200a\u200a\u3000\u2008\u2008\u2008\u3000\u3000\u3000\u3000', '\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u200a\u200a\u200a\u2008\u2008\u200a\u200a\u3000\u2008\u2008\u2008\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u200a\u200a\u200a\u2008\u2008\u200a\u200a\u3000\u2008\u2008\u2008 ✦', '\u3000\u2002\u2002\u2002\u3000\u3000\u3000,\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000*\u3000\u3000\u2008\u200a\u200a\u200a \u3000\u3000\u3000\u3000 \u3000\u3000,\u3000\u3000\u3000 \u200d \u200d \u200d \u200d \u3000 \u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000.\u3000\u3000\u3000\u3000\u3000\u2008\u3000\u3000', '\u3000\u3000\u3000.\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u2008\u3000\u200a\u200a\u2008\u2008\u2008\u2008\u2008\u2008\u2008\u200a\u200a\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000˚\u3000\u3000\u3000', '\u3000 \u2002\u2002\u3000\u3000\u3000\u3000,\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u200a\u200a\u200a\u200a\u200a\u200a\u200a\u3000\u200a\u2008\u2008\u2008:rocket:\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000.\u3000\u3000\u3000', '\u2008\u3000\u3000\u2002\u2002\u2002\u2002\u3000\u3000\u3000\u3000\u3000\u2008\u3000\u3000\u3000\u3000\u3000.\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000.\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000*', '\u3000\u3000 \u2002\u2002\u3000\u3000\u3000\u3000\u3000 ✦ \u3000\u3000\u3000\u3000\u3000\u3000\u3000\u200a\u200a\u200a\u200a\u200a\u200a\u200a\u200a\u200a\u3000\u2008\u2008\u2008\u2008\u2008\u2008\u2008\u2008\u3000\u3000\u3000\u3000', '\u3000\u3000\u2008\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u2008\u3000\u3000\u3000\u3000\u3000.\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000.\u3000\u3000\u3000\u3000\u3000\u2002\u2002\u2002\u2002\u3000\u3000.', '\u3000\u2008\u3000\u3000\u3000\u3000\u3000.\u3000\u3000\u3000\u3000 \u3000\u3000\u3000\u3000\u3000\u200a\u200a\u200a\u3000\u3000\u3000\u3000\u3000.\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000.\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u2002\u2002 \u3000', '', '\u3000˚\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000ﾟ\u3000\u3000\u3000\u3000\u3000.\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000.', '\u3000\u3000\u2008\u3000', '\u200d \u200d \u200d \u200d \u200d \u200d \u200d \u200d \u200d \u200d ,\u3000 \u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000*', '.\u3000\u3000\u3000\u3000\u3000\u2008\u3000\u3000\u3000\u3000:full_moon: \u3000\u3000\u3000\u3000\u3000\u3000\u3000:earth_americas: \u3000\u3000\u3000.\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000 ✦ \u3000\u3000\u3000\u3000\u2002\u2002 \u3000', '\u3000\u3000\u3000˚\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000*\u3000\u3000\u3000\u3000\u3000\u3000\u2008', '\u2008\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000.\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000.', '\u3000\u3000\u2008\u3000\u3000\u3000\u3000\u3000\u3000\u3000 ✦ \u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000 \u3000 \u200d \u200d \u200d \u200d \u3000\u3000\u3000\u3000 \u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000,', '']))

    @commands.command()
    @commands.has_role("Big Gay")
    async def prideflag(self, ctx):
        await ctx.send("<a:BlobLGBT_1:508331280769875978><a:BlobLGBT_2:508331280186736652>")

    @commands.command(description='For when you wanna settle the score some other way', aliases=['choice', 'pick'])
    async def choose(self, ctx, *choices):
        """Chooses between multiple choices."""
        e = discord.Embed()

        extended_choices = list(choices) * 7
        for i in range(7):
            shuffle(extended_choices)
        chosen = choice(extended_choices)

        e.add_field(name="I choose " + chosen, value=" / ".join(choices))
        await ctx.message.channel.send(embed=e)

    @commands.command()
    async def hug(self, ctx, user: discord.Member, intensity: int = 1):
        """Because everyone likes hugs

        Up to 10 intensity levels."""
        name = " *" + user.name + "*"
        if intensity <= 0:
            msg = "(っ˘̩╭╮˘̩)っ" + name
        elif intensity <= 3:
            msg = "(っ´▽｀)っ" + name
        elif intensity <= 6:
            msg = "╰(*´︶`*)╯" + name
        elif intensity <= 9:
            msg = "(つ≧▽≦)つ" + name
        elif intensity >= 10:
            msg = "(づ￣ ³￣)づ" + name + " ⊂(´・ω・｀⊂)"
        await ctx.message.channel.send(msg)

    async def _confess(self, channel, sender, text):
        text = text.lstrip("confession").lstrip("Confession").lstrip(":")

        yellowDiscord = discord.utils.get(
            self.bot.guilds, id=511989111427694632)
        isMemberOfYellow = discord.utils.get(
            yellowDiscord.members, id=sender.id)

        if type(channel) is discord.channel.DMChannel and isMemberOfYellow:
            cries_and_confessions = discord.utils.get(
                yellowDiscord.channels, id=512004850695798785)
            channel = cries_and_confessions

        e = discord.Embed()
        e.add_field(name="confession:", value=text)
        await channel.send(embed=e)

    @commands.command(aliases=['confess'])
    async def confession(self, ctx, *text):
        if type(ctx.channel) is not discord.channel.DMChannel:
            await ctx.message.delete()
        await self._confess(ctx.channel, ctx.author, " ".join(text))

    async def _answer(self, channel, question):
        e = discord.Embed()
        e.add_field(name=choice(("Yes", "No")), value=question)
        await channel.send(embed=e)

    @commands.command()
    async def answer(self, ctx, *question):
        await self._answer(ctx.channel, " ".join(question))

    @commands.Cog.listener()
    async def on_message(self, message):
        channel = message.channel

        if message.content.lower().startswith("confession"):
            if type(channel) is not discord.channel.DMChannel:
                await message.delete()
            await self._confess(channel, message.author, message.content)

        if message.content.lower().startswith("answer"):
            question = message.content.lstrip(
                "answer").lstrip("Answer").lstrip(":")
            await self._answer(channel, question)

        if ":moth:" in message.content:
            moth_emoji = list(
                filter(lambda emoji: emoji.name == "moth", self.bot.emojis))
            if moth_emoji:
                moth_emoji = moth_emoji[0]
                await message.add_reaction(moth_emoji)

    @commands.command(description="It's Russian Roulette", brief="Play some Russian Roulette", aliases=['russianroulette', 'rusroulette', 'rr'])
    async def rroulette(self, ctx):
        possible_responses = [
            'Dead',
            'Alive',
            'Alive',
            'Alive',
            'Alive'
        ]
        await ctx.message.channel.send(choice(possible_responses))

    @commands.command(aliases=['imustbang', 'whomustbangme', 'whototop', 'mustbang',
                               'ImustBang', 'whoMustBangMe', 'WhoToTop', 'mustBang'])
    async def choosePerson(self, ctx):
        """
        choosePerson | imustbang | whomustbangme | whototop
        """

        message = ctx.message
        guild = message.guild
        channel = message.channel

        response = requests.get('http://193.87.105.215/webs/discordBot/query_executor.php', params={
            'sql': "SELECT `guild_id`, `author_id`, COUNT(*) AS `count` FROM `leaderboard` WHERE `guild_id` = " + str(guild.id) + " GROUP BY `guild_id`, `author_id` ORDER BY `count` DESC"})
        message_data = json.loads(response.content)

        members_lead = list(map(lambda data: int(data["author_id"]), filter(
            lambda data: int(data["count"]) >= 500, message_data)))
        members = list(filter(
            lambda member: member.id in members_lead and not member.bot, guild.members))

        extended_choices = list(members) * 7
        for i in range(7):
            shuffle(extended_choices)
        chosen = choice(extended_choices)

        await channel.send("**I choose:** " + str(chosen))

    @commands.command()
    async def uwu(self, ctx):
        await ctx.message.delete()
        await ctx.channel.send(f"*{ctx.message.author.name}*:\n\n⎝⎠ ╲╱╲╱ ⎝⎠")

    @commands.command()
    async def ascii(self, ctx, length: int, *text):
        text = " ".join(text)

        if (length < 0):
            await ctx.send("CHYBA: Zadany parameter nie je cislo.")
            return 1

        if (length < 8):
            await ctx.send("CHYBA: Zadany parameter musi byt vacsi ako 8.")
            return 1

        letters = list(text)
        loadedLetters = len(letters)
        lettersPerRow = length // 8

        try:
            outp = "```"
            rows = 1 + ((loadedLetters - 1) // lettersPerRow)
            for bigRow in range(0, rows):
                for smallRow in range(0, 8):
                    for bigCol in range(0, lettersPerRow):
                        if (bigRow * lettersPerRow + bigCol < loadedLetters):
                            for smallCol in range(7, 0, -1):
                                letter = letters[bigRow *
                                                 lettersPerRow + bigCol]
                                value = (self.ASCII_FONT[ord(letter)][smallRow] &
                                         (1 << smallCol)) == 0
                                outp += (" " if value else "#")

                    outp += ("\n")
            outp += "```"
            await ctx.send(outp)
        except IndexError:
            await ctx.send("CHYBA: Nepovoleny znak")

    @commands.command()
    async def findegg(self, ctx):
        if 30 < random() * 100 < 50:
            egg_path = choice(glob("./data/eggs/*.png"))
            await ctx.channel.send(file=discord.File(egg_path, filename="easteregg.png"))
        else:
            grass = choice([":seedling:", ":herb:", ":tanabata_tree:", ":four_leaf_clover:", ":shamrock:"])
            await ctx.channel.send(f"{grass}")


def setup(bot):
    bot.add_cog(Fun(bot))

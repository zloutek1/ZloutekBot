
from discord.ext import commands
from random import choice, shuffle, randint
import sqlite3
from utils import dict_factory


class Gamble(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(aliases=('bank', 'bal'))
    async def balance(self, ctx):
        message = ctx.message
        channel = message.channel

        params = {
            'author_name': str(message.author),
            'author_id': message.author.id
        }

        conn = sqlite3.connect(f"data/{message.guild.id}.db")
        conn.row_factory = dict_factory
        c = conn.cursor()

        with conn:
            conn.execute("""CREATE TABLE IF NOT EXISTS bank (
                         balance real NOT NULL,
                         author_name text NOT NULL,
                         author_id integer NOT NULL)""")

            c.execute("SELECT * FROM bank WHERE author_id = :author_id", params)
            user = c.fetchone()

            if user:
                balance = user["balance"]
                await channel.send(f":bank: {message.author}'s Balance: **$ {balance}**")

            else:
                c.execute("INSERT INTO bank VALUES (0, :author_name, :author_id)", params)
                await channel.send(f":bank: {message.author}'s Balance: **$ 0.0**")

    @commands.command()
    async def slots(self, ctx):
        message = ctx.message
        channel = message.channel

        params = {
            'author_name': str(message.author),
            'author_id': message.author.id
        }

        conn = sqlite3.connect(f"data/{message.guild.id}.db")
        conn.row_factory = dict_factory
        c = conn.cursor()

        with conn:
            c.execute("SELECT * FROM bank WHERE author_id = :author_id", params)
            user = c.fetchone()
            if user:
                balance = user["balance"]
            else:
                c.execute("INSERT INTO bank VALUES (0, :author_name, :author_id)", params)
                balance = 0

            slots = ['chocolate_bar', 'bell', 'tangerine', 'apple', 'cherries', 'seven', 'trophy']
            slotValues = [slots[randint(0, len(slots) - 1)] for i in range(4)]

            slotOutput = '|\t:{}:\t|\t:{}:\t|\t:{}:\t|\t:{}:\t|\n'.format(*slotValues)
            await channel.send(slotOutput)

            if all(map(lambda slot: slot is "hundred", slotValues)):
                # JACKPOT
                balance += 60
                await channel.send("JACKPOT + **$ 60.0**")

            elif all(map(lambda slot: slot is "seven", slotValues)):
                # smaller JACKPOT
                balance += 50
                await channel.send("JACKPOT + **$ 50.0**")

            elif all(map(lambda slot: slot in ("seven", "hundred"), slotValues)):
                # mixed JACKPOT
                balance += 40
                await channel.send("JACKPOT + **$ 40.0**")

            elif all(map(lambda slot: slot is slotValues[0], slotValues)):
                # GREAT
                balance += 20
                await channel.send("GREAT + **$ 20.0**")

            elif any([s1 == s2 for i, s1 in enumerate(slotValues) for j, s2 in enumerate(slotValues) if i != j]):
                # NICE
                balance += 5
                await channel.send("NICE + **$ 5.0**")

            params["balance"] = balance
            c.execute("UPDATE bank SET balance = :balance WHERE author_id = :author_id", params)

            await channel.send(f":bank: {message.author}'s Balance: **$ {balance}**")


def setup(bot):
    bot.add_cog(Gamble(bot))

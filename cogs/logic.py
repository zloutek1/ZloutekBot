from discord.ext import commands


class Variable:
    def __init__(self, name, value=None):
        self.name = name
        self.value = value

    def __repr__(self):
        return f"I({self.name}) = {self.value};"


class Interpretation:
    def __init__(self, *known_variables):
        self.known_variables = known_variables

    def __getitem__(self, var):
        if isinstance(var, Variable):
            assert var in self.known_variables or var in globals().values(), f"unknown variable {var.name}"
            return var.value

        if isinstance(var, str):
            eq = Equasion(var)
            return eq.solve()

        if isinstance(var, Equasion):
            return var.solve()

    def __setitem__(self, var, value):
        if isinstance(var, Variable):
            assert var in self.known_variables or var in globals().values(), f"unknown variable {var.name}"
            assert value in [
                0, 1], "this logic accepts only 1 (True) or 0 (False)"
            var.value = value


class Equasion:
    BINARY = {
        '∧': lambda p, q: [[0, 0], [0, 1]][p][q],
        '∨': lambda p, q: [[0, 1], [1, 1]][p][q],
        '⇒': lambda p, q: [[1, 1], [0, 1]][p][q],
        '⇔': lambda p, q: [[1, 0], [0, 1]][p][q],
        'NAND': lambda p, q: [[1, 1], [1, 0]][p][q],
        'NOR': lambda p, q: [[1, 0], [0, 0]][p][q],
        'XOR': lambda p, q: [[0, 1], [1, 0]][p][q],
        '⊨': lambda T, φ: isComplete(T, φ)}
    UNARY = {
        '¬': lambda p: not p}
    BRACKETS = {
        '(': None,
        ')': None,
        '[': None,
        ']': None}
    SYMBOLS = {**BINARY, **UNARY, **BRACKETS}

    def __init__(self, expression):
        self.variables = []

        expression = expression.strip()
        self.expression = expression
        self.tokens = self.tokenize(expression)
        self.parse_tree = self.parse(tokens=self.tokens)

    def __repr__(self):
        return self.expression

    def nextToken(self):
        if len(self.tokens):
            self.current = self.tokens.pop(0)
        else:
            self.current = None

    def tokenize(self, expression=None):
        SYMBOLS = {**self.BINARY, **self.UNARY, **self.BRACKETS}

        expression = self.expression if expression is None else expression

        chars = list(expression)
        tokens = []

        pos = 0
        while pos < len(chars):
            c = chars[pos]
            if c is " ":
                pass
            elif c in SYMBOLS:
                tokens.append(c)
            elif c.isalpha():
                text = c
                while pos + 1 < len(chars) and chars[pos + 1].isalpha():
                    pos += 1
                    text += chars[pos]
                self.variables.append(text)
                tokens.append(text)
            else:
                raise Exception(f"Unknown symbol {c}")

            pos += 1
        return tokens

    def parse(self, tokens=None):
        """
        binary: binary ∧ uanry | binary ∨ unary | binary NAND unary | binary NOR unary | binary XOR unary | binary ⇒ unary | binary ⇔ unary | unary
        unary: ¬value | value
        value: LETTER | ( binary ) | [ binary ]
        """

        self.tokens = self.tokens if tokens is None else tokens
        self.nextToken()
        return self.binary()

    def binary(self):
        BINARY = self.BINARY.keys()

        result = self.unary()
        while self.current in BINARY:
            op = self.current
            self.nextToken()
            a = result
            b = self.unary()
            result = (op, a, b)
        return result

    def unary(self):
        UNARY = self.UNARY.keys()

        if self.current in UNARY:
            op = self.current
            self.nextToken()
            a = self.final()
            result = (op, a)
        else:
            result = self.final()
        return result

    def final(self):
        result = None

        if self.current.isalpha():
            result = (self.current)
            self.nextToken()

        elif self.current in ['(', '[', '{']:
            self.nextToken()
            result = self.binary()
            if self.current not in [')', ']', '}']:
                raise Exception("expected close bracket")
            self.nextToken()

        else:
            raise Exception("expected text or ( binary )")

        return result

    def solve(self, parse_tree=None):
        parse_tree = self.parse_tree if parse_tree is None else parse_tree

        if type(parse_tree) is str:
            if parse_tree in globals():
                if isinstance(globals()[parse_tree], Variable):
                    return I[globals()[parse_tree]]
                return globals()[parse_tree]
            return parse_tree

        if len(parse_tree) == 2:
            op, left = parse_tree
            return self.UNARY[op](self.solve(left))

        if len(parse_tree) == 3:
            op, left, right = parse_tree
            return self.BINARY[op](self.solve(left), self.solve(right))


def getVariables(set_of_equasions):
    variable_names = list(
        {variable for eq in set_of_equasions for variable in eq.variables})
    return sorted([Variable(varname) for varname in variable_names], key=lambda var: var.name)


def generateTableOfLength(n):
    for i in range(2**n):
        val = bin(i).lstrip('0b')
        yield [0 for i in range(n - len(val))] + [int(i) for i in val]


def defineVariables(variables):
    globals().update({var.name: var for var in variables})


def isCompletable(T):
    return bool(list(findModels(T)))


def isComplete(T, φ):
    models = findModels(T)
    φ = Equasion(φ)

    match = True
    for model in models:
        interp = [var for var in model if var.name in φ.variables]
        for var in interp:
            I[var] = var.value
        if not I[φ]:
            match = False
    return match


def findModels(T):
    if isinstance(T, str):
        assert T.strip().startswith("{") and T.strip().endswith(
            "}"), "set T has to be a set {...}"
        T = {Equasion(eq)
             for eq in T.strip().lstrip("{").rstrip("}").split(",")}

    elif isinstance(T, set):
        T = {Equasion(eq) if isinstance(eq, str) else eq for eq in T}

    variables = getVariables(T)
    defineVariables(variables)

    truths = generateTableOfLength(len(variables))
    for interp in truths:
        for variable, truthval in zip(variables, interp):
            I[variable] = truthval

        found = True
        for eq in T:
            result = eq.solve()
            if result == 0:
                found = False

        if found:
            yield variables

    return []

class Literal:
    def __init__(self, x):
        self.is_negative = x.strip().startswith("¬")
        self.value = x.strip().strip("¬")

    @classmethod
    def is_negative(self, literal):
        return literal.is_negative

    @classmethod
    def is_positive(self, literal):
        return not literal.is_negative

    def neg(self):
        return Literal(self.value if self.is_negative else "¬"+self.value)

    def __eq__(self, other):
        return self.value == other.value and self.is_negative == other.is_negative

    def __hash__(self):
        return hash(self.__repr__())

    def __repr__(self):
        return "¬"+self.value if self.is_negative else self.value

class Formule:
    RESOLVE_TYPES = {
        'General': 0,
        'Linear': 1,
        'LI': 2,
        'LD': 3,
        'SLD': 4,
    }

    def __init__(self, x):
        x = x.strip()
        self.formule = None

        if not (x.startswith("{") and x.endswith("}")):
            # Equasion
            self.formule = [list(map(lambda literal: Literal(literal), part.strip().strip("(").strip(")")
                                     .split("∨")))
                            for part in x.split("∧")]

        elif not (x[1:-1].lstrip().startswith("{") and x[1:-1].rstrip().endswith("}")):
            # Clausule
            self.formule = [list(map(lambda literal: Literal(literal), part.strip()
                                     .split("∨")))
                            for part in x.lstrip("{").rstrip("}")
                                         .split(",")]

        elif (x[1:-1].lstrip().startswith("{") and x[1:-1].rstrip().endswith("}")):
            # Formule
            self.formule = splitLevel(x[1:-1], ',')
            self.formule = [splitLevel(
                y[1:-1], ',') if '{' in y else splitLevel(y, ',') for y in self.formule]
            self.formule = [list(map(lambda literal: Literal(literal), clausule))
                            for clausule in self.formule]

        else:
            raise NotImplementedError("perhaps input is not a formule?")

        self.formule = [clausule for clausule in self.formule]

    def __repr__(self):
        return f"{self.formule}".replace("[", "{").replace("]", "}")

    def resolve(self, resolve_type="SLD", formule=None,
                pick_rule=lambda clausule, resolver: clausule.index(resolver)):

        formule = self.formule if formule is None else formule

        resolve_type_str = resolve_type
        if resolve_type_str not in self.RESOLVE_TYPES:
            raise NotImplementedError(
                f"Can't resolve on {resolve_type_str}, I can only resolve on {list(self.RESOLVE_TYPES.keys())}")
        resolve_type = self.RESOLVE_TYPES[resolve_type_str]

        print("INPUT", formule, "type", resolve_type_str)

        start = formule[0]

        if resolve_type >= self.RESOLVE_TYPES["LD"]:
            areThereAllNegatives = list(
                map(lambda clausule: all(map(Literal.is_negative, clausule)), formule))
            assert any(areThereAllNegatives), f"Formule is missing end clausule (all negatives"
            assert all(map(lambda clausule: list(map(Literal.is_positive, clausule)).count(True) <= 1, formule)), f"Formule has to contain maximum one positive element"

            formule = [sorted(clausule, key=Literal.is_negative)
                       for clausule in formule]
            start = formule[areThereAllNegatives.index(True)]

        print("SORTED", formule)

        resolution = list(start)
        limit = 100

        def canBeResolvedWithOn(resolution, option, resolver):
            pairsFound = 0
            for literal in option:
                if literal.neg() == resolver:
                    pairsFound += 1

            if pairsFound == 1:
                return True
            return False


        def _resolve(resolution, steps=[], counter=0):
            if counter > limit:
               return steps[:6] + [["∞"]]

            if resolution == []:
                return steps + [["□"]]

            if resolution is None:
                return steps + [["Error"]]

            for resolver in resolution:
                options = [option for option in formule if canBeResolvedWithOn(resolution, option, resolver)]
                for option in options:
                    retval = self._resolve(list(resolution), list(option), resolver, pick_rule)

                    if resolve_type <= self.RESOLVE_TYPES["LI"]:
                        retval = list(set(retval))

                    return _resolve(retval, steps+[[resolution, option]], counter+1)

        return [_resolve(resolution)]

    @staticmethod
    def _resolve(left, right, resolver, pick_rule):
        if resolver not in left:
            return None

        resolvee = right.pop(0)
        place_index = pick_rule(left, resolver)
        left.pop(place_index)
        return left + right

def splitLevel(string, sep=" "):
    """
    split string by separator only on current level
    not inside brackets.
    Example: string = "Hello, (my, wonderful), world!"
      returns ["Hello", " (my, wonderful)", " world!"]
    """

    result = []
    inBrackets = False
    openBrackets = ['(', '[', '{']
    closeBrackets = [')', ']', '}']
    lastSeparator = 0

    for i, letter in enumerate(string):
        if letter in openBrackets:
            inBrackets = True
        elif letter in closeBrackets:
            inBrackets = False
        elif not inBrackets and letter == sep:
            result.append(string[lastSeparator:i].strip())
            lastSeparator = i+1
    else:
        result.append(string[lastSeparator:i+1].strip())
    return result


def getsymbols():
    return "\t".join(Equasion.SYMBOLS.keys())

class Logic(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def getsymbols(self, ctx):
        await ctx.send(f"`{getsymbols()}`")


    @commands.command()
    async def resolve(self, ctx, resolve_type, *formule):
        formule = " ".join(formule)
        try:
            results = Formule(formule).resolve(resolve_type)
        except Exception as err:
            await ctx.send(err)
            return

        out = "```"
        for steps in results:
            for i, step in enumerate(steps):
                indent = " "*4*i
                out += f"{indent} {step}\n"
        out += "```"
        await ctx.send(out)

def setup(bot):
    bot.add_cog(Logic(bot))

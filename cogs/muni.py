import discord
import urllib.parse
from discord.ext import commands
from utils import Infix

import aiohttp
from bs4 import BeautifulSoup
import xml.etree.ElementTree as ET
import difflib

import numpy as np
import fractions

import re
import sympy


class FI_MUNI(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

        self.mathMessages = []
        self.classes = []
        self.QUIZ = {}

    async def get_math_equasion(self, equasion):
        tex2imgURL = "http://www.sciweavers.org/tex2img.php?eq={}&bc=White&fc=Black&im=png&fs=12&ff=arev&edit=0"

        embed = discord.Embed()
        embed.set_image(url=tex2imgURL.format(urllib.parse.quote(equasion)))

        return embed


    @commands.Cog.listener()
    async def on_message(self, message):
        channel = message.channel

        if message.content.startswith('$') and message.content.endswith("$"):
            equasion = message.content.strip("$")

            if len(equasion) == 0:
                return

            embed = await self.get_math_equasion(equasion)
            botResponse = await channel.send(embed=embed)

            self.mathMessages.append((message, botResponse))

    @commands.Cog.listener()
    async def on_message_edit(self, oldMessage, newMessage):
        if oldMessage.content.startswith('$') and oldMessage.content.endswith("$"):
            equasion = newMessage.content.strip("$")

            embed = await self.get_math_equasion(equasion)

            for usermsg, botmsg in self.mathMessages:
                if usermsg.id == oldMessage.id:
                    if len(equasion) == 0:
                        await botmsg.edit(content="`wring math format`", embed=None)
                        break

                    await botmsg.edit(embed=embed)
                    break

    @commands.Cog.listener()
    async def on_message_delete(self, message):
        for usermsg, botmsg in self.mathMessages:
            if usermsg.id == message.id:
                await botmsg.delete()

    async def load_subjects(self):
        if not self.classes:
            async with aiohttp.ClientSession() as session:
                async with session.get(self.PREDMETY_URL) as response:
                    soup = BeautifulSoup(response.content, features="html.parser")
                    samples = soup.find_all("h3")
                    self.classes = list(filter(lambda row: row.text[:len(row.get("id", []))] == row.get("id"), samples))

    @commands.command()
    async def inverse(self, ctx, *, matrix_block):
        message = ctx.message
        channel = message.channel

        matx = np.matrix(matrix_block.replace("\n", ";"))

        if len(set(matx.shape)) != 1:
            await channel.send("matice musi byt stverec")
            return

        inv_matx = np.linalg.inv(matx)
        matjax = "\\\\".join([" & ".join(map(lambda item: str(fractions.Fraction(item).limit_denominator()), row)) for row in inv_matx.tolist()])

        embed = await self.get_math_equasion("\\begin{{pmatrix}}{content}\\end{{pmatrix}}".format(content=matjax))

        await channel.send(embed=embed)

    @commands.command(aliases=('lsimplify', 'simplify'))
    async def logic_simplify(self, ctx, *, logic_block):
        """
        ~A                  negace
        A & B               a zaroven
        A | B               nebo
        A >> B              implikuje
        Equivalent(A, B)    ekvivalence
        """
        message = ctx.message
        channel = message.channel

        async with channel.typing():
            pattern = re.compile(r"[^\~\(\)\[\]\{\}\>\<\&\|\w\s\,]+")
            match = re.findall(pattern, logic_block)

            if match:
                await channel.send("nepovoleny znak najdeny, najdene chyby: {}".format(match))
                return

            A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z = (
                sympy.symbols("A B C D E F G H I J K L M N O P Q R S T U V W X Y Z"))

            logic_block = logic_block.replace("\n", "")
            logic_block = re.sub(r"(\[|\{)", "(", logic_block)
            logic_block = re.sub(r"(\]|\})", ")", logic_block)

            if logic_block.count("(") != logic_block.count(")"):
                await channel.send("Nerovnomerny pocet zatvoriek (otvorenych **{}** : zatvorenych **{}**)".format(logic_block.count('('), logic_block.count(')')))
                return

            Equivalent = sympy.Equivalent
            print("started simplifying for", message.author)
            simplified = sympy.simplify_logic(eval(logic_block))
            print("simplified to", simplified)

            await channel.send(simplified)

    @logic_simplify.error
    async def logic_simplify_handler(self, ctx, error):
        print("simplification error")
        if hasattr(error, 'original'):
            await ctx.send(error.original)
        else:
            await ctx.send(error)

    @commands.command(aliases=("solveset",))
    async def setsolve(self, ctx, *, set_block):
        """
        union - U or u
        intersect - ^ or n
        difference - \ or /
        equals - =
        subcet - c or C

        example:
            !setsolve
            (T u X) / (Y u Z) = (X u Y) n (Z / T)
            (X / Y) / (Z / T) = (T / X) u (Y u Z)
            (Z / T) u (X u Y) c (X / Y) / (Z / T)
            (X / Y) n (Z u T) = (X u Y) u (Z n T)
        """

        message = ctx.message
        channel = ctx.channel

        blocks = set_block.strip()
        blocks = (blocks.replace("u", "|").replace("U", "|")
                        .replace("n", "&").replace("^", "&")
                        .replace("/", "-").replace("\\", "-")
                        .replace("=", "==")
                        .replace("c", "<=").replace("C", "<="))
        blocks = blocks.split("\n")

        options = [
            (X, Y, Z, T)
            for X in range(0, 2)
            for Y in range(0, 2)
            for Z in range(0, 2)
            for T in range(0, 2)
        ]

        for option in options:
            allCorrect = True
            for equasion in blocks:
                X, Y, Z, T = option

                X = {X} if X != 0 else set()
                Y = {Y} if Y != 0 else set()
                Z = {Z} if Z != 0 else set()
                T = {T} if T != 0 else set()

                values = {"X": X, "Y": Y, "Z": Z, "T": T}
                doesMatch = eval(equasion, values)

                if not doesMatch:
                    allCorrect = False
                    break

            if allCorrect and any(map(lambda o: o != 0, option)):
                out = """
                množina X je vždy prázdná **{isXempty}**
                množina Y je vždy prázdná **{isYempty}**
                množina Z je vždy prázdná **{isZempty}**
                množina T je vždy prázdná **{isTempty}**
                """.format(isXempty=not bool(option[0]),
                           isYempty=not bool(option[1]),
                           isZempty=not bool(option[2]),
                           isTempty=not bool(option[3]))

                await channel.send("\n".join(map(lambda s: s.strip(), out.split("\n"))))

    @commands.command(aliases=("is_transitive",))
    async def matrix_transitive(self, ctx, *, matrix_block):
        message = ctx.message
        channel = message.channel

        matx = np.matrix(matrix_block.replace("\n", ";"))
        powered = matx ** 2

        matrix_string = "\\\\".join(map(lambda row: " & ".join(map(str, row)), matx.tolist()))
        embed = await self.get_math_equasion("\\begin{{pmatrix}}{content}\\end{{pmatrix}}".format(content=matrix_string))

        for row1, row2 in zip(matx.tolist(), powered.tolist()):
            for val1, val2 in zip(row1, row2):
                if val1 == 0 and val2 != 0:
                    await channel.send(embed=embed)
                    await channel.send("matica Nie je tranzitivna")
                    return

        await channel.send(embed=embed)
        await channel.send("matica Je tranzitivna")

    @commands.command()
    async def study(self, ctx):
        await ctx.message.delete()
        await ctx.send(file=discord.File("data/uc_sa.png"))


def setup(bot):
    bot.add_cog(FI_MUNI(bot))

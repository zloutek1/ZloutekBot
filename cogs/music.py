import discord
from discord.ext import commands


class Music(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def summon(self, ctx):
        author = ctx.message.author
        voice_channel = author.voice.channel
        vc = await voice_channel.connect()

        print(vc, dir(vc))


def setup(bot):
    bot.add_cog(Music(bot))

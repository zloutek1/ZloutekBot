import discord
from discord.ext import commands
import asyncio


class Polls(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.activePolls = {}

    @commands.command()
    @commands.cooldown(rate=1, per=5., type=commands.BucketType.user)
    async def simple_poll(self, ctx, *, text_block: str):
        message = ctx.message
        channel = message.channel

        if channel.id not in self.activePolls:
            question, *options = text_block.split("\n")
            options = [option.split(" ", 1) for option in options]

            await message.delete()

            description = []
            for i, option in enumerate(options):
                emoji, option = option
                description += '\n {} {}'.format(emoji, option)

            embed = discord.Embed(title=question, description=''.join(description))
            react_message = await channel.send(embed=embed)

            self.activePolls[channel.id] = {"question": question, "options": options, "message_id": react_message.id}

            for option in options:
                emoji, option = option
                await react_message.add_reaction(emoji)

            embed.set_footer(text='Poll ID: {}'.format(react_message.id))
            await react_message.edit(embed=embed)
        else:
            await channel.send("Uz prebieha anketa v tomto channeli, mozes ukoncit predosli poll pomocou !poll_results")

    @commands.command()
    async def poll_results(self, ctx, message_id: int = None):
        message = ctx.message
        channel = message.channel

        if channel.id in self.activePolls:
            message_id = message_id if message_id is not None else self.activePolls[channel.id]["message_id"]
            poll_message = await channel.get_message(message_id)

            if not poll_message.embeds:
                return
            embed = poll_message.embeds[0]

            if poll_message.author != ctx.message.guild.me:
                return

            if not embed.footer.text.startswith('Poll ID:'):
                return

            unformatted_options = [x.strip() for x in embed.description.split('\n')]
            opt_dict = {x[:2]: x[3:] for x in unformatted_options} if unformatted_options[0][0] == '1' \
                else {x[:1]: x[2:] for x in unformatted_options}

            # check if we're using numbers for the poll, or x/checkmark, parse accordingly
            voters = [ctx.message.guild.me.id]  # add the bot's ID to the list of voters to exclude it's votes

            tally = {x: 0 for x in opt_dict.keys()}
            for reaction in poll_message.reactions:
                if reaction.emoji in opt_dict.keys():
                    async for user in reaction.users():
                        if user.id not in voters:
                            tally[reaction.emoji] += 1
                            voters.append(user.id)

            output = 'Results of the poll for "**{}**":\n'.format(embed.title) + \
                     '\n'.join(['{}: {}'.format(opt_dict[key], tally[key]) for key in tally.keys()])
            await channel.send(output)
        else:
            await channel.send("Ziaden poll neprebieha")

    @commands.has_permissions(add_reactions=True)
    @commands.command(pass_context=True)
    async def poll(self, ctx, *, msg):
        """Create a poll using reactions. !help poll for more information.
        !poll <question> | <answer> | <answer> - Create a poll. You may use as many answers as you want, placing a pipe | symbol in between them.
        Example:
        !poll What is your favorite anime? | Steins;Gate | Naruto | Attack on Titan | Shrek
        You can also use the "time" flag to set the amount of time in seconds the poll will last for.
        Example:
        !poll What time is it? | HAMMER TIME! | SHOWTIME! | time=10
        """
        await ctx.message.delete()
        options = msg.split(" | ")
        time = [x for x in options if x.startswith("time=")]
        if time:
            time = time[0]
        if time:
            options.remove(time)
        if len(options) <= 1:
            return await ctx.send("You must have 2 options or more.")
        if len(options) >= 11:
            return await ctx.send("You must have 9 options or less.")
        if time:
            time = int(time.strip("time="))
        else:
            time = 30
        emoji = ['1⃣', '2⃣', '3⃣', '4⃣', '5⃣', '6⃣', '7⃣', '8⃣', '9⃣']
        to_react = []
        confirmation_msg = "**{}?**:\n\n".format(options[0].rstrip("?"))
        for idx, option in enumerate(options[1:]):
            confirmation_msg += "{} - {}\n".format(emoji[idx], option)
            to_react.append(emoji[idx])
        confirmation_msg += "\n\nYou have {} seconds to vote!".format(time)
        poll_msg = await ctx.send(confirmation_msg)
        for emote in to_react:
            await poll_msg.add_reaction(emote)
        await asyncio.sleep(time)
        async for message in ctx.message.channel.history():
            if message.id == poll_msg.id:
                poll_msg = message
        results = {}
        for reaction in poll_msg.reactions:
            if reaction.emoji in to_react:
                results[reaction.emoji] = reaction.count - 1
        end_msg = "The poll is over. The results:\n\n"
        for result in results:
            end_msg += "{} {} - {} votes\n".format(result, options[emoji.index(result) + 1], results[result])
        top_result = max(results, key=lambda key: results[key])
        if len([x for x in results if results[x] == results[top_result]]) > 1:
            top_results = []
            for key, value in results.items():
                if value == results[top_result]:
                    top_results.append(options[emoji.index(key) + 1])
            end_msg += "\nThe victory is tied between: {}".format(", ".join(top_results))
        else:
            top_result = options[emoji.index(top_result) + 1]
            end_msg += "\n{} is the winner!".format(top_result)
        await ctx.send(end_msg)


def setup(bot):
    bot.add_cog(Polls(bot))

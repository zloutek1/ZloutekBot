from discord.ext import commands
import urllib.parse


class Programming(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def regex(self, ctx, *terms):
        terms = " ".join(terms)
        channel = ctx.message.channel

        RE = urllib.parse.quote(terms)
        await channel.send("https://jex.im/regulex/#!cmd=export&flags=&re={}".format(RE))

    @commands.command()
    async def base64(self, ctx, *terms):
        pass

    @commands.command()
    async def md5(self, ctx, *terms):
        pass

    @commands.command()
    async def sha(self, ctx, *terms):
        pass

    @commands.command()
    async def url_encode(self, ctx, *terms):
        channel = ctx.message.channel
        channel.send(urllib.parse.quote(" ".join(terms)))


def setup(bot):
    bot.add_cog(Programming(bot))

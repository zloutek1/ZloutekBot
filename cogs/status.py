import sqlite3
import discord
from discord.ext import commands
from utils import dict_factory, isOwner
from random import choice
import requests
import json
import re
import asyncio
import emoji
from typing import Union, Optional


class Status(commands.Cog):
    timeFormat = '%d.%m.%Y %H:%M:%S'

    def __init__(self, bot):
        self.bot = bot
        self.suspend_leaderboard = False

    async def get_leaderboard(self, message, channel, guild_id):
        guild_id = int(guild_id if guild_id else message.guild.id)

        response = requests.get('http://193.87.105.215/webs/discordBot/query_executor.php', params={
            'sql': "SELECT `guild_id`, `author_id`, COUNT(*) AS `count` FROM `leaderboard` GROUP BY `guild_id`, `author_id` ORDER BY `count` DESC"})

        message_data = json.loads(response.content)
        return message_data

    async def get_user_leaderboard(self, message, channel, guild_id, user_id):
        guild_id = int(guild_id if guild_id else message.guild.id)
        user_id = int(user_id if user_id else message.author.id)

        response = requests.get('http://193.87.105.215/webs/discordBot/query_executor.php', params={
            'sql': "SELECT `guild_id`, `author_id`, COUNT(*) AS `count` FROM `leaderboard` WHERE `author_id` = " + str(user_id) + " GROUP BY `guild_id`, `author_id` ORDER BY `count` DESC"})

        message_data = json.loads(response.content)
        return message_data

    def get_print_leaderboard(self, message, channel, guild_id, message_data, numbered=True):
        inguild = list(filter(lambda message: int(message["guild_id"]) == guild_id, message_data))

        guild = discord.utils.get(self.bot.guilds, id=guild_id)

        out = ""
        for index, message in enumerate(inguild):
            name = discord.utils.get(guild.members, id=int(message['author_id']))
            if name is not None:
                out += "`{index} {val:>{padding}}` **{name}**\n".format(
                    index=str(index + 1) + "." if numbered else "  ",
                    val=message['count'],
                    padding=len(inguild[0]["count"]) - len(str(index + 1)) + 3,
                    name=str(name)
                )

        return out.strip()

    @commands.command()
    async def leaderboard(self, ctx, topN: Optional[int] = 20, member: Optional[discord.Member] = None, guild_id: Optional[int] = None):
        message = ctx.message
        channel = message.channel
        guild_id = int(guild_id if guild_id else message.guild.id)

        if self.suspend_leaderboard:
            zelezo = self.bot.get_emoji(540890120119910400)
            await channel.send(f"Leaderboard undergoes an update, please be patient {zelezo}", delete_after=5)
            return

        if member is not None:
            data = await self.get_user_leaderboard(message, channel, guild_id, member.id)
            out = self.get_print_leaderboard(message, channel, guild_id, data, numbered=False)
        else:
            data = await self.get_leaderboard(message, channel, guild_id)
            out = self.get_print_leaderboard(message, channel, guild_id, data)

        pager = commands.Paginator(prefix='', suffix='', max_size=1595)
        for line in out.split("\n")[:topN]:
            if len(line) > 1595:
                continue
            pager.add_line(line)

        for page in pager.pages:
            await channel.send(page)

    @commands.command()
    async def update_leaderboard(self, ctx):
        await ctx.message.delete()

        await self._update_leaderboard(ctx)

    async def _update_leaderboard(self, ctx):
        requests.get('http://193.87.105.215/webs/discordBot/query_executor.php', params={
            "sql": "DELETE FROM `leaderboard` WHERE `guild_id`=" + str(ctx.guild.id)})

        with open("data/leaderboard_all.json", 'r') as file:
            data = json.load(file)

            print("updating leaderboard?", len(data), "messages found.")

            with requests.Session() as session:
                values = []
                for message in data:
                    if self.content_is_leaderboard_countable(message['content'].lower()):
                        guild_id = message['guild_id']
                        author_id = message['author_id']

                        values.append((guild_id, author_id))

                for i in range(0, len(values), 100):
                    batch = values[i:i + 100]

                    res = session.get('http://193.87.105.215/webs/discordBot/query_executor.php', params={
                        "sql": "INSERT INTO `leaderboard` VALUES " + ",".join(map(str, batch))})
                    print("inserting batch", str(i // 100 + 1) + "/" + str(len(values) // 100), "of size", len(batch), res.content)

    @commands.command()
    async def update_emojis(self, ctx):
        await ctx.message.delete()

        await self._update_emojis(ctx)

    async def _update_emojis(self, ctx):
        requests.get('http://193.87.105.215/webs/discordBot/query_executor.php', params={
            "sql": "DELETE FROM `emojis` WHERE 1"})

        with open("data/leaderboard_all.json", 'r') as file:
            data = json.load(file)

            print("updating emojis?", len(data), "messages found.")

            with requests.Session() as session:
                values = []
                for message in data:
                    if self.content_is_leaderboard_countable(message['content'].lower()):
                        guild_id = message['guild_id']
                        author_id = message['author_id']

                        match = re.findall("(?<!\d):([a-zA-Z0-9_]{2,}):", emoji.demojize(message['content']))
                        if match:
                            for emoji_char in match:
                                values.append((guild_id, author_id, emoji_char))

                for i in range(0, len(values), 100):
                    batch = values[i:i + 100]
                    with open("data/query.txt", 'w') as fileS:
                        fileS.write("INSERT INTO `emojis` VALUES " + ",".join(map(str, batch)) + "\n\n")

                    res = session.get('http://193.87.105.215/webs/discordBot/query_executor.php', params={
                        "sql": "INSERT INTO `emojis` VALUES " + ",".join(map(str, batch))})
                    print("inserting batch", str(i // 100 + 1) + "/" + str(len(values) // 100), "of size", len(batch), res.content)

    @commands.command()
    @isOwner()
    async def update(self, ctx):
        self.suspend_leaderboard = True

        guild = ctx.guild
        message = ctx.message
        author = message.author
        ch = message.channel

        await message.delete()
        message_batch = []
        total_counter = 0

        async def get_channel_msg(channel):
            nonlocal message_batch, counter, total_counter

            await asyncio.sleep(0)
            with requests.Session() as session:
                async for message in channel.history(limit=1000000):
                    msg_data = {
                        'guild_id': guild.id,
                        'guild': str(guild),
                        'channel_id': channel.id,
                        'channel': str(channel),
                        'author_id': message.author.id,
                        'author': str(message.author),
                        'created_at': str(message.created_at),
                        'edited_at': str(message.edited_at),
                        'content': message.content,
                        'reactions': [{"name": (emoji.demojize(reaction.emoji.name)
                                                if hasattr(reaction.emoji, "name") else
                                                emoji.demojize(reaction.emoji)),
                                       "count": reaction.count,
                                       "users": list(map(
                                           lambda user: user.name,
                                           await reaction.users().flatten()
                                       ))}
                                      for reaction in message.reactions]
                    }
                    counter += 1
                    total_counter += 1

                    if self.content_is_leaderboard_countable(message.content.lower()):
                        message_batch.append(msg_data)

                    if len(message_batch) > 100:
                        values = ", ".join([str((msg["guild_id"], msg["author_id"]))
                                            for msg in message_batch])

                        res = session.get('http://193.87.105.215/webs/discordBot/query_executor.php', params={
                            "sql": "INSERT INTO `leaderboard` VALUES " + values})

                        message_batch = []

        requests.get('http://193.87.105.215/webs/discordBot/query_executor.php', params={
            "sql": "DELETE FROM `leaderboard` WHERE `guild_id`=" + str(guild.id)})

        stats = {}
        for channel in guild.channels:
            counter = 0
            if not type(channel) is discord.channel.TextChannel:
                continue

            try:
                task = await asyncio.ensure_future(get_channel_msg(channel))
                print("[Leaderboard] -", guild, ": Saved", channel)
            except discord.Forbidden:
                print("[Leaderboard] -", guild, ": Forbidden", channel)

            # print("[Leaderboard] - saved", counter, "messages")
            stats[str(channel)] = counter

        print("[Leaderboard] update done for", guild)
        channel_status_message = "\n".join([
            (channel + ":").ljust(25) + " " + str(stats[channel])
            for channel in sorted(stats, key=lambda channel: stats[channel], reverse=True)
        ])
        status_message = """
        ```
        {guild.name}
        ------------------------
        {channel_status_message}
        ------------------------
        Total: {total_counter}
        ```
        """.format(guild=guild, total_counter=total_counter, channel_status_message=channel_status_message)

        status_message = "\n".join([line.strip() for line in status_message.split("\n")])

        pager = commands.Paginator(prefix='', suffix='', max_size=1595)
        for line in status_message.split("\n"):
            if len(line) > 1595:
                continue
            pager.add_line(line)

        for page in pager.pages:
            page = "```\n" + page.strip().strip("```") + "\n```"
            await ch.send(page)

        self.suspend_leaderboard = False

    def content_is_leaderboard_countable(self, message):
        message = message.lower()
        return not (message.startswith(".") or message.startswith("rate") or
                    message.startswith("answer") or message.startswith("!") or
                    message.startswith("t!") or message.startswith("pls") or
                    message.startswith("confession"))

    @commands.Cog.listener()
    async def on_message(self, message):
        "count message sent by user in guild"

        if message.guild is not None and self.content_is_leaderboard_countable(message.content.lower()):
            guild_id = message.guild.id
            author_id = message.author.id

            with requests.Session() as session:
                session.get('http://193.87.105.215/webs/discordBot/query_executor.php', params={
                    "sql": "INSERT INTO `leaderboard` VALUES " + f"({guild_id}, {author_id})"})

                response = session.get('http://193.87.105.215/webs/discordBot/query_executor.php', params={
                    'sql': f"SELECT COUNT(*) AS `count` FROM `leaderboard` WHERE `guild_id`={guild_id} AND `author_id`={author_id}"})
                message_data = json.loads(response.content)

        if f"<@{self.bot.user.id}>" in message.content:
            options = ["👁‍🗨", "👀"]
            await message.add_reaction(choice(options))

    @commands.Cog.listener()
    async def on_member_update(self, before, after):
        pass
        """
        if str(before.status) == "online" and str(after.status) == "offline":
            print(before, before.id, "user has gone offline")

        elif str(before.status) == "offline" and str(after.status) == "online":
            print(before, before.id, "user has gone online")
        """


def setup(bot):
    bot.add_cog(Status(bot))

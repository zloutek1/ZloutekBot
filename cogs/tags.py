import random
import time
import aiohttp

from discord import Colour, Embed
from discord.ext.commands import (
    BadArgument, Bot, Cog,
    Context, Converter, group
)

from pagination import LinePaginator


class TagNameConverter(Converter):
    @staticmethod
    async def convert(ctx: Context, tag_name: str):
        def is_number(value):
            try:
                float(value)
            except ValueError:
                return False
            return True

        tag_name = tag_name.lower().strip()

        # The tag name has at least one invalid character.
        if ascii(tag_name)[1:-1] != tag_name:
            raise BadArgument("Don't be ridiculous, you can't use that character!")

        # The tag name is either empty, or consists of nothing but whitespace.
        elif not tag_name:
            raise BadArgument("Tag names should not be empty, or filled with whitespace.")

        # The tag name is a number of some kind, we don't allow that.
        elif is_number(tag_name):
            raise BadArgument("Tag names can't be numbers.")

        # The tag name is longer than 127 characters.
        elif len(tag_name) > 127:
            raise BadArgument("Are you insane? That's way too long!")

        return tag_name


class TagContentConverter(Converter):
    @staticmethod
    async def convert(ctx: Context, tag_content: str):
        tag_content = tag_content.strip()

        # The tag contents should not be empty, or filled with whitespace.
        if not tag_content:
            raise BadArgument("Tag contents should not be empty, or filled with whitespace.")

        return tag_content


class Tags(Cog):
    """
    Save new tags and fetch existing tags.
    """

    def __init__(self, bot: Bot):
        self.bot = bot
        self.tag_cooldowns = {}

    async def get_tag_data(self, guild_id: int, tag_name=None) -> dict:
        """
        Retrieve the tag_data from our API

        :param tag_name: the tag to retrieve
        :return:
        if tag_name was provided, returns a dict with tag data.
        if not, returns a list of dicts with all tag data.

        """

        params = {
            "guild_id": guild_id
        }

        if tag_name:
            params["q"] = tag_name

        """
        response = requests.get('http://193.87.105.215/webs/discordBot/tags_get.php', params=params)
        tag_data = response.json()
        """

        parameters = "&".join(f"{key}={val}" for key, val in params.items())
        URL = "http://193.87.105.215/webs/discordBot/tags_get.php?" + parameters

        async with aiohttp.ClientSession() as session:
            async with session.get(URL) as response:
                tags_data = await response.json()
                return tags_data

    async def post_tag_data(self, guild_id: int, tag_name: str, tag_content: str, author: str) -> dict:
        """
        Send some tag_data to our API to have it saved in the database.

        :param tag_name: The name of the tag to create or edit.
        :param tag_content: The content of the tag.
        :return: json response from the API in the following format:
        {
            'success': bool
        }
        """

        params = {
            'guild_id': guild_id,
            'author_id': int(str(author)[-4:]),
            'tag_name': tag_name,
            'tag_content': tag_content
        }

        """
        response = requests.get('http://193.87.105.215/webs/discordBot/tags_create.php', params=params)

        return response.json()
        """

        parameters = "&".join(f"{key}={val}" for key, val in params.items())
        URL = "http://193.87.105.215/webs/discordBot/tags_create.php?" + parameters

        async with aiohttp.ClientSession() as session:
            async with session.get(URL) as response:
                tags_data = await response.json()
                print(tags_data)
                return tags_data

    async def delete_tag_data(self, guild_id: int, tag_name: str) -> dict:
        """
        Delete a tag using our API.

        :param tag_name: The name of the tag to delete.
        :return: json response from the API in the following format:
        {
            'success': bool
        }
        """

        params = {
            "guild_id": guild_id
        }

        if tag_name:
            params["tag_name"] = tag_name

            """
            response = requests.get('http://193.87.105.215/webs/discordBot/tags_delete.php', params=params)

            return response.json()
            """

            parameters = "&".join(f"{key}={val}" for key, val in params.items())
            URL = "http://193.87.105.215/webs/discordBot/tags_delete.php?" + parameters

            async with aiohttp.ClientSession() as session:
                async with session.get(URL) as response:
                    tags_data = await response.json()
                    return tags_data

    @group(name='tags', aliases=('tag', 't'), hidden=True, invoke_without_command=True)
    async def tags_group(self, ctx: Context, *, tag_name: TagNameConverter=None):
        """Show all known tags, a single tag, or run a subcommand."""

        await ctx.invoke(self.get_command, tag_name=tag_name)

    @tags_group.command(name='get', aliases=('show', 'g'))
    async def get_command(self, ctx: Context, *, tag_name: TagNameConverter=None):
        """
        Get a list of all tags or a specified tag.

        :param ctx: Discord message context
        :param tag_name:
        If provided, this function shows data for that specific tag.
        If not provided, this function shows the caller a list of all tags.
        """

        def _command_on_cooldown(tag_name) -> bool:
            """
            Check if the command is currently on cooldown.
            The cooldown duration is set in constants.py.

            This works on a per-tag, per-channel basis.
            :param tag_name: The name of the command to check.
            :return: True if the command is cooling down. Otherwise False.
            """

            now = time.time()

            cooldown_conditions = (
                tag_name and
                tag_name in self.tag_cooldowns and
                (now - self.tag_cooldowns[tag_name]["time"]) < 5 and
                self.tag_cooldowns[tag_name]["channel"] == ctx.channel.id
            )

            if cooldown_conditions:
                return True
            return False

        if _command_on_cooldown(tag_name):
            # time_left = 5 - (time.time() - self.tag_cooldowns[tag_name]["time"])
            return

        tags = []

        embed = Embed()
        embed.colour = Colour.red()
        tag_data = await self.get_tag_data(ctx.guild.id, tag_name)

        # If we found something, prepare that data
        if tag_data:
            embed.colour = Colour.blurple()

            if tag_name:
                embed.title = tag_name

                self.tag_cooldowns[tag_name] = {
                    "time": time.time(),
                    "channel": ctx.channel.id
                }

            else:
                embed.title = "**Current tags**"

            if isinstance(tag_data, list):
                tags = [f"**»**   {tag['tag_name']}" for tag in tag_data]
                tags = sorted(tags)

            else:
                embed.description = tag_data['tag_content']

        # If not, prepare an error message.
        else:
            embed.colour = Colour.red()

            if isinstance(tag_data, dict):

                embed.description = f"**{tag_name}** is an unknown tag name. Please check the spelling and try again."
            else:
                embed.description = "**There are no tags in the database!**"

            if tag_name:
                embed.set_footer(text="To show a list of all tags, use !tags.")
                embed.title = "Tag not found."

        # Paginate if this is a list of all tags
        if tags:
            return await LinePaginator.paginate(
                (lines for lines in tags),
                ctx, embed,
                footer_text="To show a tag, type !tags <tagname>.",
                empty=False,
                max_lines=15
            )

        return await ctx.send(embed=embed)

    @tags_group.command(name='set', aliases=('add', 'edit', 's'))
    async def set_command(
        self,
        ctx: Context,
        tag_name: TagNameConverter,
        *,
        tag_content
    ):
        """
        Create a new tag or edit an existing one.

        :param ctx: discord message context
        :param tag_name: The name of the tag to create or edit.
        :param tag_content: The content of the tag.
        """

        print("ADDING?", tag_name, tag_content)
        tag_name = tag_name.lower().strip()
        tag_content = tag_content.strip()

        embed = Embed()
        embed.colour = Colour.red()
        tag_data = await self.post_tag_data(ctx.guild.id, tag_name, tag_content, ctx.message.author)
        print(tag_data)

        if tag_data.get("success"):
            embed.colour = Colour.blurple()
            embed.title = "Tag successfully added"
            embed.description = f"**{tag_name}** added to tag database."
        else:
            embed.title = "Database error"
            embed.description = ("There was a problem adding the data to the tags database. "
                                 "Please try again. If the problem persists, see the error logs.")

        return await ctx.send(embed=embed)

    @tags_group.command(name='delete', aliases=('remove', 'rm', 'd'))
    async def delete_command(self, ctx: Context, *, tag_name: TagNameConverter):
        """
        Remove a tag from the database.

        :param ctx: discord message context
        :param tag_name: The name of the tag to delete.
        """

        tag_name = tag_name.lower().strip()
        embed = Embed()
        embed.colour = Colour.red()
        tag_data = await self.delete_tag_data(ctx.guild.id, tag_name)

        if tag_data.get("success") is True:
            embed.colour = Colour.blurple()
            embed.title = tag_name
            embed.description = f"Tag successfully removed: {tag_name}."

        elif tag_data.get("success") is False:
            embed.colour = Colour.red()
            embed.title = tag_name
            embed.description = "Tag doesn't appear to exist."

        else:
            embed.title = "Database error"
            embed.description = ("There was an unexpected error with deleting the data from the tags database. "
                                 "Please try again. If the problem persists, see the error logs.")

        return await ctx.send(embed=embed)

    @get_command.error
    @set_command.error
    @delete_command.error
    async def command_error(self, ctx, error):
        if isinstance(error, BadArgument):
            embed = Embed()
            embed.colour = Colour.red()
            embed.description = str(error)
            embed.title = random.choice("Error")
            await ctx.send(embed=embed)
        else:
            pass


def setup(bot):
    bot.add_cog(Tags(bot))

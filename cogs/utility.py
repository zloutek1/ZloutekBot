import discord
from discord.ext import commands
from bs4 import BeautifulSoup
import re
from urllib.request import Request, urlopen, urlopen
import os
from utils import isOwner
import asyncio
import aiohttp


class Utility(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True, aliases=['source'])
    async def sauce(self, ctx, *, txt: str = None):
        """Find source of image. Ex: !sauce http://i.imgur.com/NIq2U67.png"""
        if txt is None:
            async for message in ctx.message.channel.history(limit=10, reverse=True):
                if message.clean_content.startswith("https://cdn.discordapp.com/attachments/"):
                    txt = message.clean_content

                if len(message.attachments) > 0:
                    txt = message.attachments[0].url

        if not txt:
            return await ctx.send('Input a link to check the source. Ex: ``>sauce http://i.imgur.com/NIq2U67.png``')
        await ctx.message.delete()

        sauce_nao = 'http://saucenao.com/search.php?db=999&url='
        request_headers = {
            "Accept-Language": "en-US,en;q=0.5",
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0",
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
            "Referer": "http://thewebsite.com",
            "Connection": "keep-alive"
        }
        loop = self.bot.loop
        try:
            req = Request(sauce_nao + txt, headers=request_headers)
            webpage = await loop.run_in_executor(None, urlopen, req)
        except Exception as e:
            return await ctx.send('Exceeded daily request limit. Try again tomorrow, sorry!')
        soup = BeautifulSoup(webpage, 'html.parser')
        pretty_soup = soup.prettify()
        em = discord.Embed(color=0xaa550f, description='**Input:**\n{}\n\n**Results:**'.format(txt))
        try:
            em.set_thumbnail(url=txt)
        except Exception as e:
            pass
        match = re.findall(r'(?s)linkify" href="(.*?)"', str(soup.find('div', id='middle')))
        title = re.findall(r'(?s)<div class="resulttitle">(.*?)</td', str(soup.find('div', id='middle')))
        similarity_percent = re.findall(r'(?s)<div class="resultsimilarityinfo">(.*?)<',
                                        str(soup.find('div', id='middle')))
        ti = ''
        if title and float(similarity_percent[0][:-1]) > 50.0:
            title = title[0].strip().replace('<br/>', '\n').replace('<strong>', '').replace('</strong>', '').replace(
                '<div class="resultcontentcolumn">', '').replace('<span class="subtext">', '\n').replace('<small>',
                                                                                                         '').replace(
                '</span>', ' ').replace('</small>', '').replace('</tr>', '').replace('</td>', '').replace('</table>',
                                                                                                          '').replace(
                '</div>', '').split('\n')
            ti = '\n'.join([i.strip() for i in title if i.strip() != ''])
            if '</a>' not in ti:
                em.add_field(name='Source', value=ti)

            try:
                pretty_soup = pretty_soup.split('id="result-hidden-notification"', 1)[0]
            except Exception as e:
                pass
            episode = re.findall(r'(?s)<span class="subtext">\n EP(.*?)<div', pretty_soup)
            ep = ''
            if episode:
                episode = episode[0].strip().replace('<br/>', '').replace('<strong>', '**').replace('</strong>',
                                                                                                    '**').replace(
                    '<span class="subtext">', '').replace('</span>', '').replace('</tr>', '').replace('</td>',
                                                                                                      '').replace(
                    '</table>', '').replace('</div>', '').split('\n')

                ep = ' '.join([j.strip() for j in episode if j.strip() != ''])
                ep = ep.replace('Est Time:', '\nEst Time:')
                em.add_field(name='More Info', value='**Episode** ' + ep, inline=False)
            est_time = re.findall(r'(?s)Est Time:(.*?)<div', pretty_soup)
            if est_time and 'Est Time:' not in ep:
                est_time = est_time[0].strip().replace('<br/>', '').replace('<strong>', '').replace('</strong>',
                                                                                                    '').replace(
                    '<span class="subtext">', '').replace('</span>', '').replace('</tr>', '').replace('</td>',
                                                                                                      '').replace(
                    '</table>', '').replace('</div>', '').split('\n')

                est = ' '.join([j.strip() for j in est_time if j.strip() != ''])
                est = est.replace('Est Time:', '\nEst Time:')
                em.add_field(name='More Info', value='**Est Time:** ' + est, inline=False)

        sources = ''
        count = 0
        source_sites = {'www.pixiv.net': 'pixiv', 'danbooru': 'danbooru', 'seiga.nicovideo': 'nico nico seiga',
                        'yande.re': 'yande.re', 'openings.moe': 'openings.moe', 'fakku.net': 'fakku',
                        'gelbooru': 'gelbooru',
                        'deviantart': 'deviantart', 'bcy.net': 'bcy.net', 'konachan.com': 'konachan',
                        'anime-pictures.net': 'anime-pictures.net', 'drawr.net': 'drawr'}
        for i in match:
            if not i.startswith('http://saucenao.com'):
                if float(similarity_percent[count][:-1]) > 50.0:
                    link_to_site = '{} - {}, '.format(i, similarity_percent[count])
                    for site in source_sites:
                        if site in i:
                            link_to_site = '[{}]({}) - {}, '.format(source_sites[site], i, similarity_percent[count])
                            break
                    sources += link_to_site
                    count += 1

            if count == 4:
                break
        sources = sources.rstrip(', ')

        material = re.search(r'(?s)Material:(.*?)</div', str(soup.find('div', id='middle')))
        if material and ('Materials:' not in ti and 'Material:' not in ti):
            material_list = material.group(1).strip().replace('<br/>', '\n').replace('<strong>', '').replace(
                '</strong>', '').split('\n')
            mat = ', '.join([i.strip() for i in material_list if i.strip() != ''])
            em.add_field(name='Material(s)', value=mat)

        characters = re.search(r'(?s)Characters:(.*?)</div', str(soup.find('div', id='middle')))
        if characters and ('Characters:' not in ti and 'Character:' not in ti):
            characters_list = characters.group(1).strip().replace('<br/>', '\n').replace('<strong>', '').replace(
                '</strong>', '').split('\n')
            chars = ', '.join([i.strip() for i in characters_list if i.strip() != ''])
            em.add_field(name='Character(s)', value=chars)

        creator = re.search(r'(?s)Creator:(.*?)</div', str(soup.find('div', id='middle')))
        if creator and ('Creators:' not in ti and 'Creator:' not in ti):
            creator_list = creator.group(1).strip().replace('<br/>', '\n').replace('<strong>', '').replace('</strong>',
                                                                                                           '').split(
                '\n')
            creat = ', '.join([i.strip() for i in creator_list if i.strip() != ''])
            em.add_field(name='Creator(s)', value=creat)

        if sources != '' and sources:
            em.add_field(name='Source sites - percent similarity', value=sources, inline=False)

        if not sources and not creator and not characters and not material and not title or float(
                similarity_percent[0][:-1]) < 50.0:
            em = discord.Embed(color=0xaa550f, description='**Input:**\n{}\n\n**No results found.**'.format(txt))

        await ctx.send(content=None, embed=em)

    @commands.command(aliases=['clearconsole', 'cc', 'clear'])
    @isOwner()
    async def cleartrace(self, ctx):
        """Clear the console."""
        if os.name == 'nt':
            os.system('cls')
        else:
            try:
                os.system('clear')
            except Exception:
                for _ in range(100):
                    print()

        message = 'Logged in as %s.' % self.bot.user
        try:
            print(message)
        except Exception as e:  # some bot usernames with special chars fail on shitty platforms
            print(message.encode(errors='replace').decode())
        await ctx.send('Console cleared successfully.', delete_after=5)

        await ctx.message.delete()

    async def on_message(self, message):
        channel = message.channel
        author = message.author

        if hasattr(channel, 'recipient'):

            for private_guild in self.bot.guilds:
                if private_guild.id == 212459103589629952:
                    for private_channel in private_guild.channels:
                        if private_channel.id == 510846250556456960:

                            embed = (discord.Embed()
                                     .set_author(name=author, icon_url=author.avatar_url)
                                     .add_field(name='Sends:', value=message.content))

                            await private_channel.send(embed=embed)

                            break
                    break

    @commands.command()
    async def doxx(self, ctx, facebook_url):
        message = ctx.message
        author = message.author
        guild = message.guild

        for channel in guild.channels:
            if str(channel) == "doxxbin":
                await message.delete()
                await channel.send("{author} {url}".format(author=author.mention, url=facebook_url))
                return

    @commands.command()
    @isOwner()
    async def backup_doxxbin(self, ctx):
        await ctx.message.delete()

        message = ctx.message
        channel = message.channel
        guild = message.guild

        doxxbin_channel = list(filter(lambda ch: ch.name == "doxxbin", guild.channels))
        if len(doxxbin_channel) != 1:
            return
        doxxbin_channel = doxxbin_channel[0]

        async for msg in doxxbin_channel.history(limit=10000):
            match = re.search("(\<\@\d+\>).*(https\:\/\/www.facebook.com\/.*)", msg.content)
            if match:
                await channel.send(match.group(1) + " " + match.group(2))

    @commands.command()
    @isOwner()
    async def doxx_status(self, ctx):
        await ctx.message.delete()

        message = ctx.message
        channel = message.channel
        guild = message.guild

        doxxbin_channel = list(filter(lambda ch: ch.name == "doxxbin-filtered", guild.channels))
        if len(doxxbin_channel) != 1:
            return
        doxxbin_channel = doxxbin_channel[0]

        doxxed = set()

        async def get_doxxed_messages():
            await asyncio.sleep(0)
            async for msg in doxxbin_channel.history(limit=10000):
                match = re.search("\<\@\!?(\d+)\>", msg.content)
                if match:
                    doxxed.add(int(match.group(1)))

        task = await asyncio.ensure_future(get_doxxed_messages())

        leftovers = set(map(lambda mem: mem.id, guild.members)) - doxxed

        out = ", ".join(map(lambda mem: "**" + mem.name + "**", filter(lambda mem: mem.id in leftovers and not mem.bot, guild.members)))
        await channel.send("Undoxxed members: " + out)

    @commands.command()
    @isOwner()
    async def say(set, ctx, *text):
        await ctx.message.delete()
        await ctx.message.channel.send(" ".join(text))

    @commands.command()
    @isOwner()
    async def edit(self, ctx, message_id, *new_content):
        await ctx.message.delete()
        channel = ctx.message.channel
        message = await channel.get_message(message_id)
        await message.edit(content=" ".join(new_content))

    async def on_member_join(self, member):
        guild = member.guild

        doxxbin_channel = list(filter(lambda ch: ch.name == "doxxbin-filtered", guild.channels))
        if len(doxxbin_channel) != 1:
            return
        doxxbin_channel = doxxbin_channel[0]

        doxxed = set()

        async def get_doxxed_messages():
            await asyncio.sleep(0)
            async for msg in doxxbin_channel.history(limit=10000):
                match = re.search("\<\@\!?(\d+)\>", msg.content)
                if match:
                    doxxed.add(int(match.group(1)))

        task = await asyncio.ensure_future(get_doxxed_messages())

        if member.id in doxxed:
            await member.add_roles(list(filter(lambda role: role.name == "Member", guild.roles)))

    @commands.command()
    async def emojiurl(self, ctx, emoji: discord.Emoji):
        await ctx.send(emoji.url)

    """
    @commands.command()
    @isOwner()
    async def saveemojis(self, ctx):
        async with aiohttp.ClientSession() as session:
            for emoji in ctx.guild.emojis:
                async with session.get(emoji.url) as resource:
                    if not os.path.exists("./data/emojis"):
                        os.makedirs("./data/emojis")

                    if not os.path.exists("./data/emojis/{guild_id}/".format(guild_id=ctx.guild.id)):
                        os.makedirs("./data/emojis/{guild_id}/".format(guild_id=ctx.guild.id))

                    output = open("./data/emojis/{guild_id}/{name}.png".format(guild_id=ctx.guild.id, name=emoji.name), "wb")
                    output.write(await resource.read())
                    output.close()
    """


def setup(bot):
    bot.add_cog(Utility(bot))

import sqlite3


# conn = sqlite3.connect("tags.db")
with conn:
    conn.execute("""CREATE TABLE IF NOT EXISTS tags (
                         guild_id integer NOT NULL,
                         tag_name text NOT NULL,
                         tag_content text NOT NULL,
                         author text NOT NULL)""")

# conn = sqlite3.connect("messages_sent.db")
with conn:
    conn.execute("""CREATE TABLE IF NOT EXISTS messages_sent (
                        guild_id integer NOT NULL,
                        author text NOT NULL,
                        count integer NOT NULL)""")

# conn = sqlite3.connect("online.db")
with conn:
    conn.execute("""CREATE TABLE IF NOT EXISTS online (
                        author text NOT NULL,
                        last_online text NOT NULL,
                        time_online text NOT NULL
)""")

# conn = sqlite3.connect("tags.db")
c = conn.cursor()
with conn:
    c.execute("SELECT * FROM tags")
    print(c.fetchall())

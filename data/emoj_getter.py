import json
import re

guild_id = 511989111427694632
topN = 10
with open("leaderboard_all.json") as file:
    messages = json.load(file)

    emoji_counter = {}
    inguild = list(filter(lambda message: message["guild_id"] == guild_id, messages))

    for msg in inguild:
        author = msg["author"]

        emojis = re.findall("\:[^\s]+\:|\:d+|\:p+", msg["content"], flags=re.IGNORECASE)
        for emoji in emojis:
            emoji_counter.setdefault(emoji, {})
            emoji_counter[emoji].setdefault("count", 0)
            emoji_counter[emoji].setdefault("users", {})
            emoji_counter[emoji]["users"].setdefault(author, 0)

            emoji_counter[emoji]["count"] += 1
            emoji_counter[emoji]["users"][author] += 1

for emoji in sorted(emoji_counter, key=lambda emoji: emoji_counter[emoji]["count"], reverse=True)[:topN]:
    users = emoji_counter[emoji]["users"]
    most_spammed_by = sorted(users, key=lambda user: users[user], reverse=True)[0]

    print(emoji, emoji_counter[emoji]["count"], most_spammed_by, sep="\t")

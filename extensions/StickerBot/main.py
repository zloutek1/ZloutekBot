import discord
from discord.ext import commands

import os
import json
import re


class StickerBot:
    stickers_filename = os.path.dirname(os.path.abspath(__file__)) + "/" + "stickers.json"

    def __init__(self, bot):
        self.bot = bot

    @staticmethod
    def isNameCorrect(name):
        return name.startswith(":") and len(name) > 2 and name.endswith(":")

    @commands.command(name='stickers', aliases=('sticker', 'stick'))
    async def stickers(self, ctx, operation, *args):
        """
        type !help stickers for more info

        operations:
            add :stickerName: stickerImageURL
            remove :stickerName:
            rename :oldName: :newName:
            list

        get stickers from https://stickersfordiscord.com/sticker-packs
        """

        if operation == "add":
            await self.addSticker(ctx, *args)

        elif operation == "remove":
            await self.removeSticker(ctx, *args)

        elif operation == "rename":
            await self.renameSticker(ctx, *args)

        elif operation == "list":
            await self.listStickers(ctx, *args)

    async def addSticker(self, ctx, stickerName, stickerURL):
        def isURLCorrect(url):
            regex = re.compile(r'^^(http)s?\:\/\/.*\/stickers\-for\-discord\/.*\.(png|jpg|gif)$', re.IGNORECASE)
            return bool(re.search(regex, url))

        message = ctx.message
        channel = message.channel
        guild = message.guild

        if not self.isNameCorrect(stickerName) or not isURLCorrect(stickerURL):
            await channel.send("Pouzi format `:stickerName: url`\na url konkretneho obrazku z https://stickersfordiscord.com/sticker-packs")
            return

        if not os.path.exists(self.stickers_filename):
            with open(self.stickers_filename, 'w') as file:
                d = dict()
                d.setdefault(str(guild.id), dict())
                file.write(json.dumps(d))

        with open(self.stickers_filename, 'r') as file:
            old_dict = file.read()
            new_dict = json.loads(old_dict)

            new_dict.setdefault(str(guild.id), dict())

            if stickerName not in new_dict[str(guild.id)]:
                new_dict[str(guild.id)][stickerName] = stickerURL
            else:
                await channel.send(f"meno {stickerName} je uz zabrane, pouzi ine alebo ho premenuj")
                return

            with open(self.stickers_filename, 'w') as file2:
                file2.write(json.dumps(new_dict))

                await channel.send(f"{stickerName} added successfully")
                await message.delete()

    async def removeSticker(self, ctx, stickerName):
        message = ctx.message
        channel = message.channel
        guild = message.guild

        if not self.isNameCorrect(stickerName):
            await channel.send("Pouzi format `:stickerName:`")
            return

        if not os.path.exists(self.stickers_filename):
            return

        with open(self.stickers_filename, 'r') as file:
            old_dict = file.read()
            new_dict = json.loads(old_dict)

            if stickerName in new_dict[str(guild.id)]:
                del new_dict[str(guild.id)][stickerName]
            else:
                await channel.send(f"meno `{stickerName}` nebolo neexistuje alebo uz bolo odstranene")
                return

            with open(self.stickers_filename, 'w') as file2:
                file2.write(json.dumps(new_dict))

                await channel.send(f"{stickerName} removed successfully")

    async def renameSticker(self, ctx, oldName, newName):
        message = ctx.message
        channel = message.channel
        guild = message.guild

        if not self.isNameCorrect(oldName) or not self.isNameCorrect(newName):
            await channel.send("Pouzi format `:stickerName:`")
            return

        if not os.path.exists(self.stickers_filename):
            return

        with open(self.stickers_filename, 'r') as file:
            old_dict = file.read()
            new_dict = json.loads(old_dict)

            if oldName in new_dict[str(guild.id)] and newName not in new_dict[str(guild.id)]:
                new_dict[str(guild.id)][newName] = new_dict[str(guild.id)][oldName]
                del new_dict[str(guild.id)][oldName]
            else:
                await channel.send(f"meno `{oldName}` neexistuje alebo meno `{newName}` uz existuje")
                return

            with open(self.stickers_filename, 'w') as file2:
                file2.write(json.dumps(new_dict))

                await channel.send(f"{oldName} renamed to {newName} successfully")

    async def listStickers(self, ctx):
        message = ctx.message
        channel = message.channel
        guild = message.guild

        with open(self.stickers_filename, 'r') as file:
            old_dict = file.read()
            new_dict = json.loads(old_dict)

            if str(guild.id) in new_dict:
                out = "**Stickers list:**\n"
                out += "```"
                for name in new_dict[str(guild.id)]:
                    out += f"{name}\n"
                out += ("no stickers" if len(new_dict[str(guild.id)]) == 0 else "")
                out += "```"

                await channel.send(out)

    async def on_message(self, message):
        channel = message.channel
        guild = message.guild
        content = message.content.strip()

        if not os.path.exists(self.stickers_filename):
            with open(self.stickers_filename, 'w') as file:
                file.write(json.dumps(dict()))

        if self.isNameCorrect(content):
            with open(self.stickers_filename, 'r') as file:
                stickers_dict = json.loads(file.read())

                if content in stickers_dict[str(guild.id)]:

                    """
                    async with aiohttp.ClientSession() as session:
                        async with session.get(stickers_dict[content]) as resp:
                            if resp.status != 200:
                                return await channel.send('Could not download file...')
                            data = io.BytesIO(await resp.read())
                            await channel.send(file=discord.File(data, f'{content}.png'))

                    await message.delete()
                    """

                    embed = (discord.Embed()
                             .set_author(name=message.author, icon_url=message.author.avatar_url)
                             .set_image(url=stickers_dict[str(guild.id)][content]))

                    await channel.send(embed=embed)
                    await message.delete()


def setup(bot):
    bot.add_cog(StickerBot(bot))

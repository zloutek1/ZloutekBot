import os
import sys
import aiohttp
import subprocess
from datetime import datetime
from discord.ext import commands
from utils import isOwner

PREFIX = "!"
TOKEN = 'MjEyNDYxMTQxNzA2ODAxMTUy.Dx5qYA.Ab6AKorkkAIsz_xipGZUQIJ9s7c'

with open("extensions.txt", 'r') as file:
    startup_extensions = [extension.strip() for extension in file]


bot = commands.Bot(command_prefix=PREFIX)
aiosession = aiohttp.ClientSession(loop=bot.loop)


@bot.command()
@isOwner()
async def restart(ctx):
    "owner command only"

    try:
        await ctx.message.delete()
        await aiosession.close(ctx)
    except Exception as e:
        pass
    await bot.logout()
    await bot.login(TOKEN)


@bot.command()
@isOwner()
async def reload(ctx):
    "owner command only"

    with open("extensions.txt", 'r') as file:
        startup_extensions = [extension.strip() for extension in file]

    for extension in startup_extensions:
        try:
            bot.unload_extension(extension)
            bot.load_extension(extension)
        except Exception as e:
            exc = '{}: {}'.format(type(e).__name__, e)
            print('Failed to reload extension {}\n{}'.format(extension, exc))

    await ctx.message.delete()


@bot.command()
async def uptime(ctx):
    channel = ctx.message.channel

    endTime = datetime.now().strftime(timeFormat)
    timeDifference = datetime.strptime(endTime, timeFormat) - datetime.strptime(startTime, timeFormat)

    await channel.send("I have been running for {}".format(timeDifference))


@bot.command()
@isOwner()
async def shutdown(ctx):
    "owner command only"

    try:
        await aiosession.close()
    except Exception as e:
        pass
    await bot.logout()


@bot.command()
async def invite(ctx):
    await ctx.message.channel.send("https://discordapp.com/oauth2/authorize?&client_id={client_id}&scope=bot".format(client_id=bot.user.id))

"""
@bot.event
async def on_command_error(error, ctx):
    if isinstance(error, commands.CommandNotFound):
        return
    else:
        print("command_error", error)
"""

if __name__ == "__main__":
    timeFormat = '%d.%m.%Y %H:%M:%S'
    startTime = datetime.now().strftime(timeFormat)

    for extension in startup_extensions:
        try:
            bot.load_extension(extension)
            print("loading extension", extension)
        except Exception as e:
            exc = '{}: {}'.format(type(e).__name__, e)
            print('Failed to load extension {}\n{}'.format(extension, exc))

    bot.run(TOKEN, reconnect=True)

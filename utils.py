import re
from discord.ext import commands


def isOwner():
    def predicate(ctx):
        return ctx.message.author.id == 107424884321218560
    return commands.check(predicate)


def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


def createTable(inp):
    """
    ',' for cell divisors
    '|' for vertical border
    '-' for horizontal border
    '\n' for new row

    Example:
    a, b, c, d | n
    --------------
    0, 0, 0, 1 | 1
    0, 0, 1, 0 | 2
    0, 1, 0, 1 | 5
    """

    rows = inp.split(";")
    sections = [re.split(r"\s?\|\s?", row.strip())
                for row in rows]
    values = [[re.split("\,\s?|\s", section.strip())
               for section in row]
              for row in sections]

    table = values

    columnLengths = []
    for row in table:
        columnID = 0
        for section in row:
            for value in section:
                if "-" not in value:
                    if columnID < len(columnLengths):
                        if len(value) > columnLengths[columnID]:
                            columnLengths[columnID] = len(value)
                    else:
                        columnLengths.append(len(value))
                    columnID += 1
                else:
                    continue

    maxSections = max([len([section for section in row]) for row in sections])

    out = ""
    for row in table:
        columnID = 0
        for section in row:
            for value in section:
                if "-" in value:
                    out += "-" * (sum(columnLengths) + len(columnLengths) + 2 * (maxSections - 1) - 1) + " "
                    columnID = len(columnLengths)
                    continue
                else:
                    if value.replace('.', '', 1).isdigit():
                        out += value.rjust(columnLengths[columnID]) + " "
                    else:
                        out += value.ljust(columnLengths[columnID]) + " "
                    columnID += 1
            out += "| "

        while columnID < len(columnLengths):
            out += " ".rjust(columnLengths[columnID]) + " "
            out += "| "
            columnID += 1

        out = out[:-2]
        out += "\n"

    return out


class Infix:
    def __init__(self, function):
        self.function = function

    def __ror__(self, other):
        return Infix(lambda x, self=self, other=other: self.function(other, x))

    def __or__(self, other):
        return self.function(other)

    def __rlshift__(self, other):
        return Infix(lambda x, self=self, other=other: self.function(other, x))

    def __rshift__(self, other):
        return self.function(other)

    def __call__(self, value1, value2):
        return self.function(value1, value2)
